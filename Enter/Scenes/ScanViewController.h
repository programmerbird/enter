//
//  ScanViewController.h
//  Enter
//
//  Created by Sittipon Simasanti on 1/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ScanViewControllerDelegate <NSObject>

-(void)scanControllerDidFailToStartSession: (id)sender;
-(void)scanController: (id)sender didReceiveBarcode: (NSString *)barcode;

@end
@interface ScanViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIView *cameraView;
@property (nonatomic, strong) IBOutlet UIButton *turnOnFlashButton;
@property (nonatomic, strong) IBOutlet UIButton *turnOffFlashButton;

@property (nonatomic, strong) IBOutlet UIView *aimBar;
@property (nonatomic, strong) IBOutlet UILabel *aimLabel;

@property (nonatomic, weak) IBOutlet id<ScanViewControllerDelegate> delegate;

-(void)startScanning;
-(void)stopScanning;

-(void)startFlashing;
-(void)stopFlashing;

-(void)reloadScanner;
@end

NS_ASSUME_NONNULL_END
