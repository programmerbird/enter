//
//  ViewController.m
//  Enter
//
//  Created by Sittipon Simasanti on 28/2/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <WebKit/WebKit.h>
#import "RootViewController.h"
#import "WebViewController.h"
#import "ScanViewController.h"
#import "SoundManager.h"

#define ANIMATE_DURATION     0.3f
#define PUSH_TO_SCAN_SECONDS 0.5f
#define SCANNER_HEIGHT      @"scannerHeight"
#define PLAY_BEEP           @"playBeep"
#define VERSION             @"version"

@interface RootViewController() {
    BOOL _isWebReady;
    BOOL _isHoldingScanButton;
    CFTimeInterval _lastStartScanTime;
}
@property (copy) dispatch_block_t webReadyBlock;
@property (nonatomic, strong) IBOutlet ScanViewController *scanController;
@property (nonatomic, strong) IBOutlet WebViewController *webController;
@property (nonatomic, strong) SoundManager *soundManager;
@end

@interface RootViewController(Scan)<ScanViewControllerDelegate>

@end

@implementation RootViewController(Scan)


-(CGFloat)scanHeight {
    
    CGFloat vh = self.view.bounds.size.height;
    NSUserDefaults *configs = [NSUserDefaults standardUserDefaults];
    CGFloat f = [[configs objectForKey: SCANNER_HEIGHT] floatValue];
    if(f <= 0){
        f = 25;
    }
    else if(f <= 15) {
        f = 15;
    }
    else if(f > 100){
        f = 100;
    }
    
    float h = vh * f / 100.0f;
    CGFloat safeBottom = self.view.safeAreaInsets.bottom;
    if(h > vh - safeBottom - 44){
        h = vh - safeBottom - 44;
    }
    return h;
}

-(void)showScanAnimated: (BOOL)animated {
    WebViewController *webController = self.webController;
    if([webController isLockingLayout]){
        animated = NO;
    }

    ScanViewController *scan = self.scanController;
    if(!scan){
        scan = [self.storyboard instantiateViewControllerWithIdentifier: @"ScanViewController"];
        [scan setDelegate: self];
        [self setScanController: scan];
    }
    CGFloat vw = self.view.bounds.size.width;
    CGFloat vh = self.view.bounds.size.height;
    CGFloat scanHeight = [self scanHeight];
    if(!animated){
        [self.view addSubview: scan.view];
        [self addChildViewController: scan];
        // [scan.view setClipsToBounds: YES];
        [scan.view setFrame:CGRectMake(0, 0, vw, scanHeight)];
        [scan startScanning];

        [self.webController.mainView setFrame: CGRectMake(0, scanHeight, vw, vh - scanHeight)];
        [self.webController layoutWithSafeAreaTop: 0];
        return;
    }
    
    [webController lockLayout];
    [webController.mainView setClipsToBounds: YES];

    UIView *scanView = self.scanController.view;
    [scanView setFrame: CGRectMake(0, -scanHeight, vw, scanHeight)];
    // [scanView setClipsToBounds: YES];
    [self.view addSubview: scan.view];
    [self addChildViewController: scan];
    
    [scan startScanning];

    [UIView animateWithDuration:ANIMATE_DURATION delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [scanView setFrame: CGRectMake(0, 0, vw, scanHeight)];
        [webController.mainView setFrame: CGRectMake(0, scanHeight, vw, vh)];
        
        [webController layoutWithSafeAreaTop: 0];
    } completion:^(BOOL finished) {
        [webController.mainView setFrame: CGRectMake(0, scanHeight, vw, vh - scanHeight)];
        [webController unlockLayout];
    }];
}

-(void)dismissScanAnimated: (BOOL)animated {
    
    WebViewController *webController = self.webController;
    if([webController isLockingLayout]){
        animated = NO;
    }
    ScanViewController *scan = self.scanController;
    if(!scan) {
        return;
    }
    
    
    [self setScanController: nil];
    CGFloat vw = self.view.bounds.size.width;
    CGFloat vh = self.view.bounds.size.height;
    if(!animated){
        [scan stopScanning];
        [scan.view removeFromSuperview];
        [scan removeFromParentViewController];
        
        [webController unlockLayout];
        [webController.mainView setFrame: CGRectMake(0, 0, vw, vh)];
        return;
    }
    

    CGFloat scanHeight = scan.view.frame.size.height;
    CGFloat safeTop = self.view.safeAreaInsets.top;

    [webController lockLayout];
    [webController.mainView setFrame: CGRectMake(0, webController.mainView.frame.origin.y, vw, vh)];
    [UIView animateWithDuration:ANIMATE_DURATION delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [scan.view setFrame: CGRectMake(0, -scanHeight, vw, scanHeight)];
        [webController.mainView setFrame: CGRectMake(0, 0, vw, vh)];
        [webController layoutWithSafeAreaTop: safeTop];
        
    } completion:^(BOOL finished) {
        [scan stopScanning];
        [scan.view removeFromSuperview];
        [scan removeFromParentViewController];
        [webController unlockLayout];
        [webController.mainView setFrame: CGRectMake(0, 0, vw, vh)];
    }];
}

-(void)scanController:(id)sender didReceiveBarcode:(NSString *)barcode {
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:PLAY_BEEP]){
        [self.soundManager playSound: SoundGoodBeep];
    }
    [self.webController didReceiveBarcode: barcode];
    if(!_isHoldingScanButton){
        [self dismissScanAnimated: YES];
    }
}

-(void)scanControllerDidFailToStartSession:(id)sender {
    
}


@end
@interface RootViewController(Web)<WebViewControllerDelegate>

@end
@implementation RootViewController(Web)

-(BOOL)webControllerWillEnterAddress:(id)sender {
    CGFloat scanHeight = self.scanController.view.frame.size.height;
    CGFloat viewHeight = self.view.bounds.size.height;
    if(scanHeight >= viewHeight / 2.0f){
        [self dismissScanAnimated: YES];
    }
    return YES;
}
-(BOOL)webControllerWillRestoreLastSession: (id)sender {
    _isWebReady = YES;
    if(self.webReadyBlock != NULL){
        dispatch_block_t b = self.webReadyBlock;
        self.webReadyBlock = NULL;
        
        dispatch_async(dispatch_get_main_queue(), b);
        return NO;
    }
    return YES;
}

-(void)webControllerDidTouchDownScan:(id)sender {
    if(self.scanController){
        _isHoldingScanButton = NO;
        [self dismissScanAnimated: YES];
        return;
    }
    
    _isHoldingScanButton = YES;
    _lastStartScanTime = CACurrentMediaTime();
    [self showScanAnimated: YES];
    
    
}
-(void)webControllerDidTouchUpScan:(id)sender {
    
    _isHoldingScanButton = NO;
    if(self.scanController){
        CFTimeInterval elapsedTime = CACurrentMediaTime() - _lastStartScanTime;
        if(elapsedTime > PUSH_TO_SCAN_SECONDS){
            [self dismissScanAnimated: YES];
        }
        return;
    }

}

@end


@implementation RootViewController


-(void)onReady: (dispatch_block_t)block {
    if(_isWebReady){
        block();
        return;
    }
    self.webReadyBlock = block;
}

-(void)viewDidLayoutSubviews {
    if([self.webController isLockingLayout]){
        return;
    }
    if(self.scanController){
        CGFloat vw = self.view.bounds.size.width;
        CGFloat vh = self.view.bounds.size.height;
        
        CGFloat scanHeight = [self scanHeight];
        [self.scanController.view setFrame: CGRectMake(0, 0, vw, scanHeight)];
        [self.webController.mainView setFrame: CGRectMake(0, scanHeight, vw, vh-scanHeight)];
        [self.scanController reloadScanner];
        return;
    }
    
    [self.webController.mainView setFrame: self.view.bounds];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSUserDefaults *configs = [NSUserDefaults standardUserDefaults];
    NSInteger version = [configs integerForKey:VERSION];
    if(version <= 0) {
        [configs setInteger:1 forKey:VERSION];
        [configs setBool:YES forKey:PLAY_BEEP];
        [configs synchronize];
    }
    
    WebViewController *web = self.webController;
    if(!web) {
        web = [self.storyboard instantiateViewControllerWithIdentifier: @"WebViewController"];
        [web setDelegate: self];
        [self setWebController: web];
    }
    
    [self.view addSubview: web.view];
    [self addChildViewController: web];
    [web.view setFrame: self.view.bounds];
    [web.mainView setFrame: web.view.bounds];
    [web layoutWithSafeAreaTop: self.view.safeAreaInsets.top];

    SoundManager *soundManager = [[SoundManager alloc] init];
    [self setSoundManager: soundManager];
    [soundManager loadSounds];
}


-(void)viewSafeAreaInsetsDidChange {
    [super viewSafeAreaInsetsDidChange];
    
    [self.webController.mainView setFrame: self.view.bounds];
    if(self.scanController){
        [self.webController layoutWithSafeAreaTop: 0];
    }
    else {
        CGFloat safeTop = self.view.safeAreaInsets.top;
        [self.webController layoutWithSafeAreaTop: safeTop];
    }
    
}

-(void)loadWeb:(NSURL *)url {
    [self.webController loadWeb: url.absoluteString];
}

-(void)loadSearch: (NSString*)query engine: (nullable NSString*)engine {
    [self.webController loadSearch:query engine:engine];
}
@end
