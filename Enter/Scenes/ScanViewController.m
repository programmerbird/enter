//
//  ScanViewController.m
//  Enter
//
//  Created by Sittipon Simasanti on 1/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "ScanViewController.h"
#import "MTBBarcodeScanner.h"
#define SCANNER_CAMERA          @"scannerCamera"
#define SCANNER_CAMERA_FRONT    @"front"
#define SCANNER_ZOOM_FACTOR     @"scannerZoomFactor"
#define SCANNER_FLASH           @"scannerFlash"

@interface ScanViewController () {
    
    BOOL _isFlashing;
    BOOL _isCameraScanning;
    BOOL _isCameraPreparing;
    
    CFTimeInterval _usedBarcodeTime;
    
}

@property (nonatomic, strong) NSString *usedBarcode;
@property (nonatomic, strong) IBOutlet MTBBarcodeScanner *scanner;

@end

@implementation ScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(self.scanner){
        return;
    }

    _isFlashing = NO;
    _isCameraScanning = NO;
    _isCameraPreparing = NO;
    [self.aimLabel setText: nil];
    

    if(![self canFlashing]){
        [self.turnOnFlashButton setHidden: YES];
        [self.turnOffFlashButton setHidden: YES];
    }
    else {
        [self.turnOnFlashButton setHidden: NO];
        [self.turnOffFlashButton setHidden: YES];
    }
    
}

-(void)setPreparing: (BOOL)value {
    _isCameraPreparing = value;
}

-(void)startScanning {
    if(_isCameraScanning){
        return;
    }
    _isCameraScanning = YES;
    [self setUsedBarcode: nil];
    
    MTBBarcodeScanner *scanner = self.scanner;
    if(!scanner) {
        scanner = [[MTBBarcodeScanner alloc] initWithPreviewView: self.cameraView];
        [self setScanner: scanner];
    }
    


    __weak ScanViewController *weakSelf = self;
    [self setPreparing: YES];
    dispatch_async(dispatch_get_main_queue(), ^{
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            [weakSelf setPreparing: NO];
            
            if(!success) {
                [weakSelf.delegate scanControllerDidFailToStartSession: weakSelf];
                return;
            }
            
            @try {
                
                NSError *error = nil;
                [scanner setCamera: MTBCameraBack error:&error];
                [scanner setDidStartScanningBlock:^{
                    NSUserDefaults *configs = [NSUserDefaults standardUserDefaults];
                    NSString *scannerCamera = [configs objectForKey: SCANNER_CAMERA];
                    if([scannerCamera isEqualToString: SCANNER_CAMERA_FRONT]){
                        NSError *e = nil;
                        [weakSelf.scanner setCamera: MTBCameraFront error:&e];
                    }
                    
                    CGFloat zoomFactor = [[configs objectForKey: SCANNER_ZOOM_FACTOR] floatValue];
                    if(zoomFactor <= 0){
                        zoomFactor = 1.5f;
                    }
                    
                    BOOL useFlash = [[configs objectForKey: SCANNER_FLASH] boolValue];

                    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeVideo];
                    [device lockForConfiguration: nil];
                    [device setVideoZoomFactor: zoomFactor];
                    if(useFlash){
                        [weakSelf setFlashing: YES];
                        [device setTorchMode: AVCaptureTorchModeOn];
                    }
                    else {
                        [weakSelf setFlashing: NO];
                        [device setTorchMode: AVCaptureTorchModeOff];
                    }
                    [device unlockForConfiguration];
                }];
                
                [scanner startScanningWithResultBlock:^(NSArray<AVMetadataMachineReadableCodeObject *> *codes) {
                    [weakSelf barcodesDidScan: codes];
                } error:&error];
                
                [weakSelf scanningDidStart];
            }
            @catch(NSException *exception){
                
            }


        }];
    });
}

-(void)barcodesDidScan:(NSArray<AVMetadataMachineReadableCodeObject *>*)codes {
    if(!_isCameraScanning) {
        return;
    }
    
    AVMetadataMachineReadableCodeObject *code = [codes firstObject];
    NSString *barcode = code.stringValue;
    if(!barcode) {
        return;
    }
    if([barcode isEqualToString: @"(null)"]){
        return;
    }
    
    CFTimeInterval now = CACurrentMediaTime();
    if([barcode isEqualToString: self.usedBarcode]){
        CFTimeInterval elapsedTime = CACurrentMediaTime() - _usedBarcodeTime;
        _usedBarcodeTime = now;
        if(elapsedTime < 1){
            return;
        }
    }
    _usedBarcodeTime = now;
    [self setUsedBarcode: barcode];
    
    __weak ScanViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf showAimWithBarcode: barcode];
        [weakSelf.delegate scanController: weakSelf didReceiveBarcode: barcode];
    });
}

-(void)showAimWithBarcode: (NSString*)barcode{
    [self.aimBar setAlpha: 1.0f];
    [self.aimLabel setText: barcode];
    
    UIView *aimBar = self.aimBar;
    [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [aimBar setAlpha: 0.2f];
    } completion:^(BOOL finished) {
    }];
}


-(void)scanningDidStart {
    if(!_isCameraScanning){
        [self.scanner stopScanning];
        return;
    }
}

-(void)stopScanning {
    if(_isFlashing){
        [self stopFlashing];
    }
    if(!_isCameraScanning){
        return;
    }
    _isCameraScanning = NO;
    if(!_isCameraPreparing){
        [self.scanner stopScanning];
    }
    [self.cameraView setHidden: YES];
}

-(void)setFlashing: (BOOL)value {
    _isFlashing = value;
    
    __weak ScanViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.turnOnFlashButton setHidden: value];
        [weakSelf.turnOffFlashButton setHidden: !value];
    });
}

-(BOOL)canFlashing {
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeVideo];
    return [device hasTorch];
}

-(void)stopFlashing {
    if(!_isFlashing){
        return;
    }
    _isFlashing = NO;
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeVideo];
    if(![device hasTorch]){
        return;
    }
    [device lockForConfiguration: nil];
    [device setTorchMode: AVCaptureTorchModeOff];
    [device unlockForConfiguration];
    
    __weak ScanViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.turnOnFlashButton setHidden: NO];
        [weakSelf.turnOffFlashButton setHidden: YES];
    });
}

-(void)startFlashing {
    if(_isFlashing){
        return;
    }
    _isFlashing = YES;
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeVideo];
    if(![device hasTorch]){
        return;
    }
    [device lockForConfiguration: nil];
    [device setTorchMode: AVCaptureTorchModeOn];
    [device unlockForConfiguration];

    __weak ScanViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.turnOnFlashButton setHidden: YES];
        [weakSelf.turnOffFlashButton setHidden: NO];
    });
}

-(IBAction)turnOnFlashDidTap: (id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:SCANNER_FLASH];
    [self startFlashing];
}
-(IBAction)turnOffFlashDidTap: (id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:SCANNER_FLASH];
    [self stopFlashing];
}

-(void)reloadScanner {
    @try{
        [self.scanner setScanRect: self.view.bounds];
    }
    @catch(NSException *exception){
        
    }
}
@end
