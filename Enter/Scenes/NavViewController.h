//
//  NavigationViewController.h
//  Enter
//
//  Created by Sittipon Simasanti on 14/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol NavViewController <NSObject>

-(void)navigationControllerWantDismiss: (id)sender;
-(void)navigationControllerDidTapBackButton: (id)sender;
-(void)navigationControllerDidTapForwardButton: (id)sender;
-(void)navigationControllerDidTapRefreshButton: (id)sender;
-(void)navigationControllerDidTapShareButton: (id)sender;
-(void)navigationControllerDidTapBookmarkButton: (id)sender;

@end
@interface NavViewController : UIViewController

@property (nonatomic, weak) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, weak) IBOutlet id<NavViewController> delegate;

@property (nonatomic, weak) IBOutlet UIButton *backButton;
@property (nonatomic, weak) IBOutlet UIButton *forwardButton;
@property (nonatomic, weak) IBOutlet UIButton *refreshButton;
@property (nonatomic, weak) IBOutlet UIButton *shareButton;
@property (nonatomic, weak) IBOutlet UIButton *bookmarkButton;

-(void)showBalloonFromRect: (CGRect)rect;

@end

NS_ASSUME_NONNULL_END
