//
//  ViewController.h
//  Enter
//
//  Created by Sittipon Simasanti on 28/2/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController 

-(void)loadWeb: (NSURL* _Nonnull)url;
-(void)loadSearch: (NSString* _Nonnull)query engine: (nullable NSString*)engine;

-(void)onReady: (dispatch_block_t _Nonnull )block;

@end

