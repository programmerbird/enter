//
//  BookmarkViewController.m
//  Enter
//
//  Created by Sittipon Simasanti on 2/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "BookmarkViewController.h"
#import "BookmarkCell.h"
#import "Bookmark.h"
#import "BookmarkManager.h"
#import "FaviconFinder.h"

#define SAFE_TOP 100

@interface BookmarkViewController ()

@property (nonatomic, strong) dispatch_queue_t queue;
@property (nonatomic, strong) FaviconFinder *finder;
@property (nonatomic, strong) BookmarkManager *bookmarkManager;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, weak) UILongPressGestureRecognizer *longPressGesture;

@property (nonatomic, weak) UIPanGestureRecognizer *panGesture;

-(void)startMoveSessionForIndexPath: (NSIndexPath*)indexPath;

@end




@interface BookmarkViewController(CollectionDataSource)<UICollectionViewDataSource>
@end
@implementation BookmarkViewController(CollectionDataSource)


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.bookmarkManager.bookmarks count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BookmarkCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookmarkCell" forIndexPath:indexPath];
    Bookmark *bookmark = [self.bookmarkManager.bookmarks objectAtIndex:indexPath.row];
    [cell.label setText: bookmark.title];
    [cell.iconView.layer setMasksToBounds: YES];
    [cell.iconView.layer setCornerRadius: 8];
    [cell.iconView setHidden: YES];
    
    [cell.letterLabel.layer setMasksToBounds: YES];
    [cell.letterLabel.layer setCornerRadius: 8];
    if([bookmark.title length] > 1) {
        NSString *letter = [bookmark.title substringToIndex: 1];
        [cell.letterLabel setText: letter];
        [cell.letterLabel setHidden: NO];
    }
    else {
        [cell.letterLabel setText: @"-"];
    }
    
    FaviconFinder *finder = self.finder;
    dispatch_async(self.queue, ^{
        NSURL *url = [NSURL URLWithString: bookmark.url];
        // [finder removeImageCacheForURL: url];
        
        UIImage *image = [finder iconImageForURL: url];
        if(!image){
            return;
        }
        
        CGFloat w = image.size.width;
        dispatch_async(dispatch_get_main_queue(), ^{
            if(![cell.label.text isEqualToString: bookmark.title]){
                return;
            }
            if(w * 2.0f >= cell.iconView.frame.size.width){
                [cell.iconView setContentMode: UIViewContentModeScaleAspectFit];
            }
            else {
                [cell.iconView setContentMode: UIViewContentModeCenter];
            }
            [cell.iconView setImage: image];
            [cell.iconView setHidden: NO];
            [cell.letterLabel setHidden: YES];
        });
    });
    return cell;
}
 

// The view that is returned must be retrieved from a call to -dequeueReusableSupplementaryViewOfKind:withReuseIdentifier:forIndexPath:
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"Header" forIndexPath:indexPath];
    return cell;
}



/// Returns a list of index titles to display in the index view (e.g. ["A", "B", "C" ... "Z", "#"])
- (nullable NSArray<NSString *> *)indexTitlesForCollectionView:(UICollectionView *)collectionView {
    return nil;
}

/// Returns the index path that corresponds to the given title / index. (e.g. "B",1)
/// Return an index path with a single index to indicate an entire section, instead of a specific item.
- (NSIndexPath *)collectionView:(UICollectionView *)collectionView indexPathForIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
    return nil;
}



@end

@interface BookmarkViewController(CollectionDelegate)<UICollectionViewDelegate>
@end
@implementation BookmarkViewController(CollectionDelegate)
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if([self.panGesture isEnabled]){
        return NO;
    }
    return YES;
}
- (BOOL)collectionView:(UICollectionView *)collectionView shouldDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Bookmark *bookmark = [self.bookmarkManager.bookmarks objectAtIndex:indexPath.row];
    
    [self.delegate bookmarkController:self didSelectBookmark:bookmark];

}



- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath*)destinationIndexPath {
    

    BookmarkManager *manager = self.bookmarkManager;
    Bookmark *bookmark = [manager.bookmarks objectAtIndex: sourceIndexPath.row];
    [manager.bookmarks removeObjectAtIndex: sourceIndexPath.row];
    [manager.bookmarks insertObject:bookmark atIndex:destinationIndexPath.row];
    dispatch_async(self.queue, ^{
        [manager writeToFile];
    });
    
    return;
}


- (UIContextMenuConfiguration *)collectionView:(UICollectionView *)collectionView contextMenuConfigurationForItemAtIndexPath:(NSIndexPath *)indexPath point:(CGPoint)point API_AVAILABLE(ios(13.0)){
    
    if([self.panGesture isEnabled]){
        return nil;
    }
    
    BookmarkManager *manager = self.bookmarkManager;
    Bookmark *bookmark = [manager.bookmarks objectAtIndex:indexPath.row];

    __weak BookmarkViewController *weakSelf = self;
    
    UIAction *copyAction = [UIAction actionWithTitle:NSLocalizedString(@"Copy URL", nil) image:[UIImage systemImageNamed:@"doc.on.doc"] identifier:nil handler:^(__kindof UIAction * _Nonnull action) {
        NSURL *url = [NSURL URLWithString: bookmark.url];
        [[UIPasteboard generalPasteboard] setURL: url];
    }];

    UIAction *rearrangeAction = [UIAction actionWithTitle:NSLocalizedString(@"Rearrange", nil) image:[UIImage systemImageNamed:@"square.grid.3x2.fill"] identifier:nil handler:^(__kindof UIAction * _Nonnull action) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf startMoveSessionForIndexPath: indexPath];
        });
    }];

    UIAction *deleteAction = [UIAction actionWithTitle:NSLocalizedString(@"Delete", nil) image:[UIImage systemImageNamed:@"trash"] identifier:nil handler:^(__kindof UIAction * _Nonnull action) {
        
        [weakSelf.collectionView deleteItemsAtIndexPaths:@[indexPath]];
        [manager.bookmarks removeObjectAtIndex: indexPath.row];
        dispatch_async(weakSelf.queue, ^{
            [manager writeToFile];
        });
    }];
    
    [deleteAction setAttributes: UIMenuElementAttributesDestructive];

    UIMenu *menu = [UIMenu menuWithTitle:@"" children:@[
        copyAction,
        rearrangeAction,
        deleteAction
    ]];
    
    return [UIContextMenuConfiguration configurationWithIdentifier:nil previewProvider:nil actionProvider:^UIMenu * _Nullable(NSArray<UIMenuElement *> * _Nonnull suggestedActions) {
        return menu;
    }];
}
@end

@interface BookmarkViewController(CollectionFlowLayout)<UICollectionViewDelegateFlowLayout>
@end
@implementation BookmarkViewController(CollectionFlowLayout)

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
 
    return CGSizeMake(80, 100);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 20, 0, 20);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(100, 64);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

@end

@interface BookmarkViewController(Scroll)<UIScrollViewDelegate>
@end
@implementation BookmarkViewController(Scroll)
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.delegate bookmarkControllerDidScroll: self];
}
@end


@implementation BookmarkViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.collectionView setAlwaysBounceVertical: YES];

    FaviconFinder *finder = self.finder;
    if(!finder) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dir = [[paths firstObject] stringByAppendingPathComponent: @"favicon"];
        finder = [[FaviconFinder alloc] initWithDirectory: dir];
        [self setFinder: finder];
    }

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *bookmarkPath = [[paths firstObject] stringByAppendingPathComponent:@"Bookmarks.json"];
    [self setQueue: BookmarkManager.queue];

    BookmarkManager *manager = [[BookmarkManager alloc] initWithFile: bookmarkPath];
    [self setBookmarkManager: manager];
    
    __weak BookmarkViewController *weakSelf = self;
    dispatch_async(self.queue, ^{
        [manager restoreFromFile];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.collectionView reloadData];
        });
    });
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [self.collectionView addGestureRecognizer: panGesture];
    [self setPanGesture: panGesture];
    [panGesture setEnabled: NO];

    if (@available(ios 13, *)) {
    }
    else {
        UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
        [self.collectionView addGestureRecognizer:longPressGesture];
        [self setLongPressGesture: longPressGesture];
    }
    

}

-(CAAnimation*)shakeAnimation {
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];

    CGFloat wobbleAngle = 0.06f;

    NSValue* valLeft = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(wobbleAngle, 0.0f, 0.0f, 1.0f)];
    NSValue* valRight = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(-wobbleAngle, 0.0f, 0.0f, 1.0f)];
    animation.values = [NSArray arrayWithObjects:valLeft, valRight, nil];

    animation.autoreverses = YES;
    animation.duration = 0.125;
    animation.repeatCount = HUGE_VALF;
    return animation;
}


-(void)startWiggle {
    for (UIView *view in self.collectionView.visibleCells){
        [view.layer addAnimation:[self shakeAnimation] forKey:@""];
    }
}

-(void)stopWiggle {
    for (UIView *view in self.collectionView.visibleCells){
        [view.layer removeAllAnimations];
    }
}

-(void)handleLongPressGesture: (UILongPressGestureRecognizer *)longPressGesture {
    
    if(longPressGesture.state != UIGestureRecognizerStateBegan){
        return;
    }
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:[longPressGesture locationInView:self.collectionView]];
    
    if (indexPath == nil) {
        [longPressGesture setEnabled: NO];
        [longPressGesture setEnabled: YES];
        return;
    }
    
    

    [self showActionSheetForIndexPath: indexPath];
}

-(void)showActionSheetForIndexPath: (NSIndexPath*)indexPath {
    Bookmark *bookmark = [self.bookmarkManager.bookmarks objectAtIndex: indexPath.row];

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:bookmark.title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    

    UIAlertAction *copyURL = [UIAlertAction actionWithTitle:NSLocalizedString(@"Copy URL", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // copy url
        NSURL *url = [NSURL URLWithString: bookmark.url];
        [[UIPasteboard generalPasteboard] setURL: url];
    }];
    [alert addAction: copyURL];

    __weak BookmarkViewController *weakSelf = self;
    BookmarkManager *manager = self.bookmarkManager;
    
    UIAlertAction *rearrangeItemAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Rearrange", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // rearrange
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf startMoveSessionForIndexPath: indexPath];
        });
    }];
    [alert addAction: rearrangeItemAction];

    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        // delete
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [manager.bookmarks removeObjectAtIndex: indexPath.row];
            [weakSelf.collectionView reloadData];
            dispatch_async(weakSelf.queue, ^{
                [manager writeToFile];
            });
        });
    }];
    [alert addAction: deleteAction];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:^{
        }];
    }];
    [alert addAction: cancelAction];
    
    [self presentViewController:alert animated:YES completion:^{
    }];
    return;
}

-(void)handlePanGesture: (UIPanGestureRecognizer *)panGesture {
    
    switch (panGesture.state) {
        case UIGestureRecognizerStateBegan: {
            return;
        }
        case UIGestureRecognizerStateChanged: {
            CGPoint point = [panGesture locationInView: self.collectionView];
            [self.collectionView updateInteractiveMovementTargetPosition:point];
            return;
        }
        case UIGestureRecognizerStateEnded: {
            [self.collectionView endInteractiveMovement];
            [self stopWiggle];
            [self.panGesture setEnabled: NO];
            [self.longPressGesture setEnabled: YES];
            return;
        }
        default: {
            [self.collectionView cancelInteractiveMovement];
            return;
        }
    }
}

-(void)startMoveSessionForIndexPath: (NSIndexPath*)indexPath {
    
    Bookmark *bookmark = [self.bookmarkManager.bookmarks objectAtIndex: indexPath.row];
    [self.delegate bookmarkController: self willRearrangeBookmark: bookmark];
    
    [self startWiggle];
    [self.longPressGesture setEnabled: NO];
    [self.panGesture setEnabled: YES];
    [self.collectionView beginInteractiveMovementForItemAtIndexPath: indexPath];
    
    
}


-(void)setVisibleFrame: (CGRect)frame {
    [self.view setFrame: CGRectMake(frame.origin.x, frame.origin.y - SAFE_TOP, frame.size.width, frame.size.height + SAFE_TOP)];
}




@end

