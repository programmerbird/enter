//
//  BookmarkViewController.h
//  Enter
//
//  Created by Sittipon Simasanti on 2/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bookmark.h"

NS_ASSUME_NONNULL_BEGIN

@protocol BookmarkViewControllerDelegate <NSObject>

-(void)bookmarkController: (id)sender didSelectBookmark: (Bookmark*)bookmark;
-(void)bookmarkControllerDidScroll: (id)sender;
-(void)bookmarkController:(id)sender willRearrangeBookmark: (Bookmark*)bookmark;

@end
@interface BookmarkViewController : UIViewController
@property (nonatomic, weak) IBOutlet id<BookmarkViewControllerDelegate> delegate;
-(void)setVisibleFrame: (CGRect)frame;
@end

NS_ASSUME_NONNULL_END
