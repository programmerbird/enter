//
//  NavigationViewController.m
//  Enter
//
//  Created by Sittipon Simasanti on 14/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "NavViewController.h"

@interface NavViewController ()

@property (nonatomic, strong) IBOutlet UIView *targetView;
@property (nonatomic, strong) IBOutlet UIView *containerView;

@end

@implementation NavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(CGPoint)middleBottomFromRect: (CGRect)rect {
    CGFloat x = rect.origin.x + rect.size.width / 2.0f;
    CGFloat y = rect.origin.y + rect.size.height;
    return CGPointMake(x, y);
}

-(void)showBalloonFromRect: (CGRect)rect {
    
    CGRect currentRect = [self.view convertRect:self.targetView.bounds fromView:self.targetView];
    CGPoint current = [self middleBottomFromRect: currentRect];
    CGPoint target = [self middleBottomFromRect: rect];
    
    CGFloat dx = target.x - current.x;
    CGFloat dy = target.y - current.y;
    
    CGPoint o = self.containerView.frame.origin;
    [self.containerView setFrame: CGRectMake(o.x + dx, o.y + dy, self.containerView.frame.size.width, self.containerView.frame.size.height)];
    
}

-(IBAction)backgroundDidTap:(id)sender {
    [self.delegate navigationControllerWantDismiss: self];
}

-(IBAction)backDidTap: (id)sender {
    [self.delegate navigationControllerDidTapBackButton: self];
}
-(IBAction)forwardDidTap: (id)sender {
    [self.delegate navigationControllerDidTapForwardButton: self];
}
-(IBAction)refreshDidTap: (id)sender {
    [self.delegate navigationControllerDidTapRefreshButton: self];
}
-(IBAction)shareDidTap: (id)sender {
    [self.delegate navigationControllerDidTapShareButton: self];
}
-(IBAction)bookmarkDidTap: (id)sender {
    [self.delegate navigationControllerDidTapBookmarkButton: self];
}
@end
