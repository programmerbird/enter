//
//  WebViewController.h
//  Enter
//
//  Created by Sittipon Simasanti on 29/2/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "FullScreenWebView.h"
#import "RichLabel.h"
#import "QueryBar.h"

NS_ASSUME_NONNULL_BEGIN
@protocol WebViewControllerDelegate <NSObject>

-(void)webControllerDidTouchDownScan: (id)sender;
-(void)webControllerDidTouchUpScan: (id)sender;
-(BOOL)webControllerWillRestoreLastSession: (id)sender;
-(BOOL)webControllerWillEnterAddress: (id)sender;

@end
@interface WebViewController : UIViewController

@property (nonatomic, weak) IBOutlet id<WebViewControllerDelegate> delegate;


@property (nonatomic, weak) IBOutlet UIView *mainView;
@property (nonatomic, weak) IBOutlet UIButton *backButton;
@property (nonatomic, weak) IBOutlet UIButton *forwardButton;
@property (nonatomic, weak) IBOutlet UIButton *reloadButton;
@property (nonatomic, weak) IBOutlet UIButton *shareButton;


@property (nonatomic, weak) IBOutlet UIView *topBarBackground;
@property (nonatomic, weak) IBOutlet UIView *bottomBarBackground;
@property (nonatomic, weak) IBOutlet UIView *topBarButtons;
@property (nonatomic, weak) IBOutlet UIView *bottomBarButtons;
@property (nonatomic, weak) IBOutlet UIView *errorView;
@property (nonatomic, weak) IBOutlet UILabel *errorLabel;
@property (nonatomic, weak) IBOutlet FullScreenWebView *webView;

@property (nonatomic, weak) IBOutlet UIButton *moreButton;
@property (nonatomic, weak) IBOutlet UIButton *scanButton;
@property (nonatomic, weak) IBOutlet RichLabel *richLabel;

@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet QueryBar *queryBar;

-(void)loadWebOrSearch: (NSString *)term;
-(void)loadSearch: (NSString *)term engine: (nullable NSString*)engine;
-(void)loadWeb: (NSString *)term;

-(void)didReceiveBarcode: (NSString*)barcode;

@end

@interface WebViewController(Layout)<UIScrollViewDelegate>
-(void)lockLayout;
-(void)unlockLayout;
-(void)layoutWithSafeAreaTop: (CGFloat)safeTop;
-(BOOL)isLockingLayout;
@end


NS_ASSUME_NONNULL_END
