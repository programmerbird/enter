//
//  WebViewController.m
//  Enter
//
//  Created by Sittipon Simasanti on 29/2/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <WebKit/WebKit.h>
#import "WebViewController.h"
#import "QueryBar.h"
#import "SearchManager.h"
#import "FakeProgressView.h"
#import "BookmarkViewController.h"
#import "NavViewController.h"
#import "AddToBookmarkActivity.h"
#import "OpenInSafariActivity.h"


#define DEFAULT_HOME_PAGE   @"https://www.google.com"
#define LAUNCH_URL          @"launchURL"

#define BAR_HEIGHT          44
#define ANIMATE_DURATION    0.3f
#define SAFE_TOP            100
#define BUTTON_WIDTH        60


@interface WebViewController() {
    BOOL _isLockingLayout;
    
    CFTimeInterval _startScanTime;
}

@property (nonatomic, strong) BookmarkViewController *bookmarkController;
@property (nonatomic, strong) NavViewController *navigationController;

@property (nonatomic, weak) IBOutlet FakeProgressView *progressView;
@property (nonatomic, strong) SearchManager *searchManager;
@property (nonatomic, strong, nullable) NSString *searchHost;
@property (nonatomic, strong, nullable) NSString *searchQuery;
@property (nonatomic, strong, nullable) NSString *temporaryQuery;
@end


@interface WebViewController(Bookmark)<BookmarkViewControllerDelegate>
@end
@implementation WebViewController(Bookmark)

-(void)bookmarkController:(id)sender didSelectBookmark:(Bookmark *)bookmark {
    
    [self loadWeb: bookmark.url];
    [self.queryBar dismissTextFieldAnimated: YES];
    [self.webView becomeFirstResponder];
}

-(void)showBookmarkAnimated: (BOOL)animated {
    
    BookmarkViewController *book = self.bookmarkController;
    if(!book){
        book = [self.storyboard instantiateViewControllerWithIdentifier: @"BookmarkViewController"];
        [book setDelegate: self];
        [self setBookmarkController: book];
    }
    
    UIView *bottomBar = self.bottomBarBackground;
    UIView *bottomButtons = self.bottomBarButtons;

    CGFloat vw = self.view.bounds.size.width;
    CGFloat vh = self.view.bounds.size.height;
    CGFloat safeTop = self.view.safeAreaInsets.top;
    [book setVisibleFrame: CGRectMake(0, safeTop + BAR_HEIGHT, vw, vh - safeTop - BAR_HEIGHT)];
    
    if(!animated){
        [self.view insertSubview:book.view belowSubview:self.topBarBackground];
        [self addChildViewController:book];
        [bottomBar setHidden: YES];
        [bottomButtons setHidden: YES];
        [book.view setAlpha: 1];
        [book.view setHidden: NO];
        return;
    }
    
    [book.view setAlpha: 0];
    [book.view setHidden: NO];
    [self.mainView insertSubview:book.view belowSubview:self.topBarBackground];
    [self addChildViewController:book];

    [UIView animateWithDuration:ANIMATE_DURATION delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [book.view setAlpha: 1];
        [bottomBar setAlpha: 0];
        [bottomButtons setAlpha: 0];
    } completion:^(BOOL finished) {
        [bottomBar setHidden: YES];
        [bottomButtons setHidden: YES];
    }];
}


-(void)dismissBookmarkAnimated: (BOOL)animated {
    BookmarkViewController *book = self.bookmarkController;
    if(!book) {
        return;
    }
    UIView *bottomBar = self.bottomBarBackground;
    UIView *bottomButtons = self.bottomBarButtons;

    [self setBookmarkController: nil];
    if(!animated){
        [bottomBar setAlpha: 1];
        [bottomButtons setAlpha: 1];
        [bottomBar setHidden: NO];
        [bottomButtons setHidden: NO];
        [book.view removeFromSuperview];
        [book removeFromParentViewController];
        return;
    }
    [bottomBar setAlpha: 0];
    [bottomButtons setAlpha: 0];
    [bottomBar setHidden: NO];
    [bottomButtons setHidden: NO];

    [UIView animateWithDuration:ANIMATE_DURATION delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [book.view setAlpha: 0];
        [bottomBar setAlpha: 1];
        [bottomButtons setAlpha: 1];
    } completion:^(BOOL finished) {
        [book.view removeFromSuperview];
        [book removeFromParentViewController];
    }];
}

-(void)bookmarkControllerDidScroll:(id)sender {
    [self.queryBar.textField resignFirstResponder];
}

-(void)bookmarkController:(id)sender willRearrangeBookmark:(Bookmark *)bookmark {
    [self.queryBar.textField resignFirstResponder];
}

@end

@interface WebViewController(WebKit)<WKNavigationDelegate>
@end
@implementation WebViewController(Webkit)

-(void)reloadTitleLabel {
    
    WKWebView *webView = self.webView;
    
    if([self.temporaryQuery length] > 0){
        [self.richLabel setIsLock: NO];
    }
    else {
        [self.richLabel setIsLock: webView.hasOnlySecureContent];

        if([self.searchQuery length] > 0){
            NSURL *url = webView.URL;
            if(url && ![url.host isEqualToString:self.searchHost]){
                [self setSearchQuery: nil];
            }
        }
        
    }
    
    if([self.searchQuery length] > 0){
        [self.richLabel setIsSearch: YES];
        [self.richLabel setPrefixText: nil];
        [self.richLabel setMainText: self.searchQuery];
        [self.richLabel setPostfixText: nil];
    }
    else if([self.temporaryQuery length] > 0){
        [self.richLabel setIsSearch: NO];

        NSString *text = self.temporaryQuery;
        if(![text containsString: @"://"]){
            text = [NSString stringWithFormat: @"http://%@", text];
        }
        
        NSURL *url = [NSURL URLWithString: text];
        NSString *hostName = url.host;
        
        if(![hostName hasPrefix: @"www."]){
            [self.richLabel setPrefixText: nil];
        }
        else {
            [self.richLabel setPrefixText: @"www."];
            hostName = [hostName substringFromIndex:4];
        }
        [self.richLabel setMainText: hostName];
        [self.richLabel setPostfixText: nil];

    }
    else {
        [self.richLabel setIsSearch: NO];

        NSURL *url = webView.URL;
        NSString *hostName = url.host;
        
        if(![hostName hasPrefix: @"www."]){
            [self.richLabel setPrefixText: nil];
        }
        else {
            // [self.richLabel setPrefixText: @"www."];
            [self.richLabel setPrefixText: nil];
            hostName = [hostName substringFromIndex:4];
        }
        [self.richLabel setMainText: hostName];
        [self.richLabel setPostfixText: nil];
    }
    
    [self.richLabel reloadLayout];

    if(![self.richLabel isHidden]){
        CGFloat vw = self.richLabel.superview.bounds.size.width;
        CGFloat rw = self.richLabel.bounds.size.width;
        CGFloat rh = self.richLabel.bounds.size.height;
        
        CGFloat left  =(vw - rw) / 2.0f;
        if(left <= 0) {
            left = 0;
        }
        [self.richLabel setFrame: CGRectMake(left, 0, rw, rh)];
    }
}

-(void)reloadHistoryButtons {
    [self.backButton setEnabled: self.webView.canGoBack];
    [self.forwardButton setEnabled: self.webView.canGoForward];
}
-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [self.progressView startLoading];
    [self reloadHistoryButtons];
}
-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [self.errorView setHidden: NO];
    [self.errorLabel setText: error.localizedDescription];
    [self.progressView stopLoading];
    [self reloadHistoryButtons];
}
-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [self.errorView setHidden: NO];
    [self.errorLabel setText: error.localizedDescription];
    [self.progressView stopLoading];
    [self reloadHistoryButtons];
}
-(void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    if(self.temporaryQuery){
        [self setSearchHost: self.webView.URL.host];
    }
    [self setTemporaryQuery: nil];
    [self.errorView setHidden: YES];
    [self reloadTitleLabel];
    [self reloadHistoryButtons];
}
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if(self.temporaryQuery){
        [self setSearchHost: self.webView.URL.host];
    }
    [self setTemporaryQuery: nil];
    [self.errorView setHidden: YES];
    [self reloadTitleLabel];
    [self.progressView stopLoading];
    [self reloadHistoryButtons];
}


-(BOOL)isURL: (NSString *)term {
    if([term containsString: @"."]){
        return YES;
    }
    if([term containsString: @"://"]){
        return YES;
    }
    if([term hasPrefix: @"http://"]){
        return YES;
    }
    if([term hasPrefix: @"https://"]){
        return YES;
    }
    if([term hasPrefix: @"localhost"]){
        return YES;
    }
    return NO;
}


-(IBAction)submitDidTap: (id)sender {

    NSString *term = self.textField.text;
    [self loadWebOrSearch: term];
    [self.queryBar.textField resignFirstResponder];
    [self.queryBar dismissTextFieldAnimated: YES];
    [self.webView becomeFirstResponder];
    
}

@end


@implementation WebViewController(Layout)
-(void)lockLayout {
    _isLockingLayout += 1;
}
-(void)unlockLayout {
    _isLockingLayout -= 1;
    if(_isLockingLayout <= 0){
        _isLockingLayout = 0;
    }
}
-(BOOL)isLockingLayout {
    return _isLockingLayout > 0;
}
-(void)layoutWithSafeAreaTop: (CGFloat)safeTop {

    
    CGFloat vh = self.view.bounds.size.height;
    CGFloat vw = self.view.bounds.size.width;
    
    CGFloat mh = self.mainView.bounds.size.height;
    [self.topBarButtons setFrame: CGRectMake(0, safeTop, vw, BAR_HEIGHT)];
    [self.topBarBackground setFrame: CGRectMake(0, 0, vw, safeTop + BAR_HEIGHT)];

    CGFloat safeBottom = self.view.superview.safeAreaInsets.bottom;
    [self.bottomBarButtons setFrame: CGRectMake(0, vh - safeBottom - BAR_HEIGHT, vw, BAR_HEIGHT)];
    [self.bottomBarBackground setFrame: CGRectMake(0, vh - safeBottom - BAR_HEIGHT, vw, safeBottom + BAR_HEIGHT)];

    [self.webView setVisibleFrame:
     CGRectMake(0, safeTop + BAR_HEIGHT, vw,
                mh - safeTop - BAR_HEIGHT - safeBottom - BAR_HEIGHT)];
    
    // CGFloat gap = (vw - BUTTON_WIDTH * 5.0f) / 4.0f;
    
    CGFloat gap = (vw - BUTTON_WIDTH * 4.0f) / 3.0f;
    CGFloat cursor = 0;
    [self.backButton setFrame: CGRectMake(cursor, 0, BUTTON_WIDTH, BAR_HEIGHT)];
    cursor += gap + BUTTON_WIDTH;
    [self.forwardButton setFrame: CGRectMake(cursor, 0, BUTTON_WIDTH, BAR_HEIGHT)];
    cursor += gap + BUTTON_WIDTH;
    [self.scanButton setFrame: CGRectMake(cursor, 0, BUTTON_WIDTH, BAR_HEIGHT)];
    cursor += gap + BUTTON_WIDTH;
    [self.shareButton setFrame: CGRectMake(vw - BUTTON_WIDTH, 0, BUTTON_WIDTH, BAR_HEIGHT)];
    [self.moreButton setHidden: YES];
    
    // [self.shareButton setFrame: CGRectMake(cursor, 0, BUTTON_WIDTH, BAR_HEIGHT)];
    // [self.moreButton setFrame: CGRectMake(vw - BUTTON_WIDTH, 0, BUTTON_WIDTH, BAR_HEIGHT)];
    // [self.reloadButton setFrame: CGRectMake(vw - BUTTON_WIDTH, 0, BUTTON_WIDTH, BAR_HEIGHT)];

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(_isLockingLayout > 0){
        return;
    }
    
}
-(void)viewDidLayoutSubviews {
    if(_isLockingLayout > 0){
        return;
    }
    
    [super viewDidLayoutSubviews];
}

-(void)viewSafeAreaInsetsDidChange {
    if(_isLockingLayout > 0){
        return;
    }
    [super viewSafeAreaInsetsDidChange];
}


@end

@interface WebViewController (QueryBar)<QueryBarDelegate>
@end

@implementation WebViewController(QueryBar)
-(void)queryBar:(id)sender willDismissTextField:(UITextField *)textField {
    
    [self dismissBookmarkAnimated: YES];
    [self.webView becomeFirstResponder];
}
-(void)queryBar:(id)sender willShowTextField:(UITextField *)textField {
    
    [self.delegate webControllerWillEnterAddress: self];
    [self showBookmarkAnimated: YES];
    
    if(self.temporaryQuery) {
        [textField setText: self.temporaryQuery];
        return;
    }
    
    NSURL *url = self.webView.URL;
    if(self.searchQuery && [url.host isEqualToString: self.searchHost]){
        [textField setText: self.searchQuery];
    }
    else {
        [textField setText: url.absoluteString];
    }
}
@end

@interface WebViewController(FakeProgress)<FakeProgressViewDelegate>
@end
@implementation WebViewController(FakeProgress)
-(BOOL)fakeProgressCanEstimate:(id)sender {
    return YES;
}
-(CGFloat)fakeProgressEstimateProgress:(id)sender {
    return [self.webView estimatedProgress];
}

@end

@interface WebViewController(Navigation)<NavViewController>
@end

@implementation WebViewController(Navigation)

-(void)navigationControllerWantDismiss:(id)sender {
    [self dismissNavigationAnimated: YES];
    
}
-(void)dismissNavigationAnimated: (BOOL)animated {
    NavViewController *nav = self.navigationController;
    if(!nav){
        return;
    }
    
    [self setNavigationController: nil];
    if(!animated) {
        [nav.view removeFromSuperview];
        [nav removeFromParentViewController];
        return;
    }
    
    [UIView animateWithDuration:ANIMATE_DURATION delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [nav.view setAlpha: 0];
    } completion:^(BOOL finished) {
        [nav.view removeFromSuperview];
        [nav removeFromParentViewController];
    }];
    
}
-(void)showNavigationAnimated: (BOOL)animated {
    if(self.navigationController){
        return;
    }
    
    NavViewController *nav = self.navigationController;
    if(!nav){
        nav = [self.storyboard instantiateViewControllerWithIdentifier:@"NavViewController"];
        [nav setDelegate: self];
        [self addChildViewController: nav];
        [self setNavigationController: nav];
    }

    [nav.view setFrame: self.view.bounds];
    [self.view addSubview: nav.view];

    [nav.backButton setEnabled: self.webView.canGoBack];
    [nav.forwardButton setEnabled: self.webView.canGoForward];

    CGRect rect = [self.view convertRect:self.moreButton.bounds fromView:self.moreButton];
    [nav showBalloonFromRect: rect];
    if(!animated){
        return;
    }
    
    [nav.view setAlpha: 0];
    [UIView animateWithDuration:ANIMATE_DURATION delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [nav.view setAlpha: 1.0];
    } completion:^(BOOL finished) {
    }];
}


-(void)navigationControllerDidTapBackButton: (id)sender {
    [self dismissNavigationAnimated: YES];
    [self.webView goBack];
    [self.webView becomeFirstResponder];
}
-(void)navigationControllerDidTapForwardButton: (id)sender {
    [self dismissNavigationAnimated: YES];
    [self.webView goForward];
    [self.webView becomeFirstResponder];
}
-(void)navigationControllerDidTapRefreshButton: (id)sender {
    
    UIButton *button = self.navigationController.refreshButton;
    [UIView animateWithDuration:ANIMATE_DURATION delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [button setTransform: CGAffineTransformMakeRotation(M_PI)];
    } completion:^(BOOL finished) {
    }];
    
    [self dismissNavigationAnimated: YES];
    [self.webView reload];
    [self.webView becomeFirstResponder];

}


-(void)showShareSheet {
    NSURL *url = self.webView.URL;
    
    AddToBookmarkActivity *addBookmark = [[AddToBookmarkActivity alloc] init];
    [addBookmark setViewController: self];
    [addBookmark setWebTitle: self.webView.title];
    
    OpenInSafariActivity *openInSafari = [[OpenInSafariActivity alloc] init];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[
            url,
        ] applicationActivities:@[
            addBookmark,
            openInSafari,
        ]];
    
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    __weak WKWebView *webView = self.webView;
    [activityViewController setCompletionWithItemsHandler:^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        [webView becomeFirstResponder];
    }];
    
    [self presentViewController:activityViewController animated:YES completion:^{
    }];
}

-(void)navigationControllerDidTapShareButton: (id)sender {
    [self dismissNavigationAnimated: YES];
    [self showShareSheet];
}

-(void)navigationControllerDidTapBookmarkButton: (id)sender {
    
}

@end
@interface WebViewController(UI)<WKUIDelegate>
@end
@implementation WebViewController(UI)

-(WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {
    
    if(!navigationAction.targetFrame.isMainFrame){
        [webView loadRequest: navigationAction.request];
        return nil;
    }
    return nil;
}
@end
@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _isLockingLayout = 0;
    
    [self.webView.scrollView setDelegate: self];
    [self.webView setNavigationDelegate: self];
    [self.webView setUIDelegate: self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
    
    if([self.delegate webControllerWillRestoreLastSession: self]){
        NSString *url = [[NSUserDefaults standardUserDefaults] objectForKey:LAUNCH_URL];
        if([url length] <= 0){
            url = DEFAULT_HOME_PAGE;
        }
        [self loadWeb: url];
    }
    
}


-(void)appWillResignActive {
    NSUserDefaults *configs = [NSUserDefaults standardUserDefaults];
    NSString *url = [self.webView.URL absoluteString];
    [configs setObject:url forKey:LAUNCH_URL];
    [configs synchronize];
}

-(void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [super viewWillDisappear: animated];
}


-(IBAction)backButtonDidTap: (id)sender {
    [self.webView goBack];
    [self.webView becomeFirstResponder];
}

-(IBAction)forwardButtonDidTap: (id)sender {
    [self.webView goForward];
    [self.webView becomeFirstResponder];
}
-(IBAction)reloadButtonDidTap: (id)sender {
    UIButton *button = self.reloadButton;
    [UIView animateWithDuration:ANIMATE_DURATION
                          delay:0
                        options:(UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseIn)
                     animations:^{
        [button setTransform: CGAffineTransformMakeRotation(M_PI)];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:ANIMATE_DURATION
                              delay:0
                            options:(UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseOut)
                         animations:^{
            [button setTransform: CGAffineTransformMakeRotation(M_PI * 2.0f)];
        } completion:^(BOOL finished) {
            
        }];
    }];
    [self.webView reload];
    [self.webView becomeFirstResponder];
}

-(IBAction)shareButtonDidTap: (id)sender {
    [self showShareSheet];
}

-(IBAction)scanDidTouchDown: (id)sender {
    [self.delegate webControllerDidTouchDownScan: self];
}

-(IBAction)scanDidTouchUp: (id)sender {
    [self.delegate webControllerDidTouchUpScan: self];
}
-(IBAction)scanDidTap: (id)sender {
    // [self.delegate webControllerDidTapScan: self];
}

-(IBAction)moreDidTap:(id)sender {
    [self showNavigationAnimated: YES];
}
-(void)loadWeb: (NSString*)term {
    [self setSearchQuery: nil];
    [self setTemporaryQuery: term];
    [self reloadTitleLabel];

    if(![term containsString: @"://"]) {
        term = [NSString stringWithFormat:@"http://%@", term];
    }
    NSURL *url = [NSURL URLWithString:term];
    if([url.scheme isEqualToString: @"http"] || [url.scheme isEqualToString: @"https"]){
        NSURLRequest *request = [NSURLRequest requestWithURL: url];
        [self.webView loadRequest: request];
        return;
    }
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
    }];
}

-(void)loadSearch: (NSString *)term engine: (nullable NSString*)engine{
    [self setSearchQuery: term];
    [self setTemporaryQuery: term];
    [self reloadTitleLabel];

    SearchManager *searchManager = self.searchManager;
    if(!searchManager){
        searchManager = [[SearchManager alloc] init];
        [self setSearchManager: searchManager];
    }
    NSURL *url = [searchManager urlForTerm:term engine:engine];
    NSURLRequest *request = [NSURLRequest requestWithURL: url];
    [self.webView loadRequest: request];
}

-(void)loadWebOrSearch: (NSString *)term {
    if([self isURL: term]){
        [self loadWeb: term];
    }
    else {
        [self loadSearch: term engine: nil];
    }
}


-(void)didReceiveBarcode:(NSString *)barcode {
    
    if(self.bookmarkController){
        [self loadWebOrSearch: barcode];
        [self.queryBar.textField resignFirstResponder];
        [self.queryBar dismissTextFieldAnimated: YES];
        [self.webView becomeFirstResponder];
        return;
    }

    if([barcode hasPrefix:@"http://"]){
        [self loadWeb: barcode];
        return;
    }
    if([barcode hasPrefix:@"https://"]){
        [self loadWeb: barcode];
        return;
    }
    if([barcode containsString:@"://"]){
        NSURL *url = [NSURL URLWithString: barcode];
        if(url){
            [[UIApplication sharedApplication] openURL:url
                                               options:@{}
                                     completionHandler:^(BOOL success) {
            }];
            return;
        }
    }
    
    WKWebView *webView = self.webView;
    NSString *script = [NSString stringWithFormat: @"onbarcode(\"%@\");", barcode];
    [webView evaluateJavaScript:script completionHandler:^(id sender, NSError *error) {
    }];

}

@end
