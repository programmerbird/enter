//
//  FaviconFinder.h
//  Enter
//
//  Created by Sittipon Simasanti on 15/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface FaviconFinder : NSObject

-(id)init;
-(id)initWithDirectory: (NSString *)directory;

-(NSURL*)responseURLFromURL: (NSURL*)url;
-(NSString*)htmlFromURL: (NSURL *)url;
-(NSString*)linkFromTag: (NSString*)tag;
-(NSString*)titleFromHTML: (NSString*)html;

-(NSArray *)faviconURLsFromHTML: (NSString*) html relativeToURL: (NSURL*)url;
-(NSArray *)appleIconURLsFromHTML: (NSString*)html relativeToURL: (NSURL*)url;

-(UIImage*)largestImageFromURLs:(NSArray*)imageURLs;

-(void)removeImageCacheForURL: (NSURL*)url;
-(UIImage*)iconImageForURL: (NSURL*)url;

@end

NS_ASSUME_NONNULL_END
