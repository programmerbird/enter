//
//  BookmarkManager.h
//  Enter
//
//  Created by Sittipon Simasanti on 16/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BookmarkManager : NSObject
@property (nonatomic, strong) NSMutableArray *bookmarks;

+(dispatch_queue_t)queue;
-(id)initWithFile: (NSString *)path;

-(void)restoreFromFile;
-(void)writeToFile;
@end

NS_ASSUME_NONNULL_END
