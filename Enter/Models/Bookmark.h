//
//  Bookmark.h
//  Enter
//
//  Created by Sittipon Simasanti on 3/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Bookmark : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *url;

-(id)initWithTitle: (NSString*)title url: (NSString*)url;

@end

NS_ASSUME_NONNULL_END
