//
//  SoundManager.m
//  Enter
//
//  Created by Sittipon Simasanti on 2/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "SoundManager.h"
#import <AudioToolbox/AudioToolbox.h>

typedef NS_ENUM(NSInteger, State){
    StateIdle,
    StateLoaded,
};

@implementation SoundManager {
    SystemSoundID _goodBeep;
    SystemSoundID _badBeep;
    State _state;
}

-(id)init {
    
    self = [super init];
    if(self){
        _state = StateIdle;
    }
    return self;
}

-(void)loadSounds{
    if(_state == StateLoaded){
        return;
    }

    _state = StateLoaded;
    [self loadSound: &_goodBeep withName: @"good"];
    [self loadSound: &_badBeep withName: @"bad"];
}

-(void)loadSound: (SystemSoundID *)sound withName: (NSString *)soundName {
    
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:soundName ofType:@"caf"];
    if(!soundPath){
        *sound = 0;
        return;
    }
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:soundPath], sound);
}

-(void)playSound: (Sound)sound {
    if(_state != StateLoaded){
        [self loadSounds];
    }
    switch(sound){
        case SoundGoodBeep: {
            AudioServicesPlayAlertSound(_goodBeep);
            return;
        }
        case SoundBadBeep: {
            AudioServicesPlayAlertSound(_badBeep);
            return;
        }
    }
    
}
-(void)unloadSounds {
    if(_state != StateLoaded){
        return;
    }
    _state = StateIdle;
    AudioServicesDisposeSystemSoundID(_goodBeep);
    AudioServicesDisposeSystemSoundID(_badBeep);
}

-(void)dealloc {
    [self unloadSounds];
}
@end
