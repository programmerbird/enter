//
//  SoundManager.h
//  Enter
//
//  Created by Sittipon Simasanti on 2/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, Sound){
    SoundGoodBeep,
    SoundBadBeep,
};

@interface SoundManager : NSObject
-(void)loadSounds;
-(void)playSound: (Sound)sound;
@end

NS_ASSUME_NONNULL_END
