//
//  BookmarkManager.m
//  Enter
//
//  Created by Sittipon Simasanti on 16/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "BookmarkManager.h"
#import "Bookmark.h"

@interface BookmarkManager()
@property (nonatomic, strong) NSString *path;
@end
@implementation BookmarkManager


+(dispatch_queue_t)queue {
    static dispatch_once_t queueCreationGuard;
    static dispatch_queue_t queue;
    dispatch_once(&queueCreationGuard, ^{
        queue = dispatch_queue_create("com.programmerbird.Enter.bookmarkQueue", 0);
    });
    return queue;
}

-(id)initWithFile: (NSString *)path {

    self = [super init];
    if(self) {
        [self setPath: path];
        
        NSMutableArray *entries = [[NSMutableArray alloc] init];
        [self setBookmarks: entries];
    }
    return self;
}

-(void)restoreFromFile {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath: self.path]){
        
        NSMutableArray *bookmarks = [[NSMutableArray alloc] init];
        Bookmark *n = [[Bookmark alloc] initWithTitle:@"Google" url:@"https://www.google.com"];
        [bookmarks addObject: n];
        Bookmark *w = [[Bookmark alloc] initWithTitle:@"Wikipedia" url:@"https://www.wikipedia.org"];
        [bookmarks addObject: w];
        Bookmark *f = [[Bookmark alloc] initWithTitle:@"Facebook" url:@"https://www.facebook.com"];
        [bookmarks addObject: f];
        Bookmark *a = [[Bookmark alloc] initWithTitle:@"Amazon" url:@"https://www.amazon.com"];
        [bookmarks addObject: a];
        Bookmark *r = [[Bookmark alloc] initWithTitle:@"Reddit" url:@"https://www.reddit.com"];
        [bookmarks addObject: r];
        Bookmark *t = [[Bookmark alloc] initWithTitle:@"Twitter" url:@"https://www.twitter.com"];
        [bookmarks addObject: t];
        [self setBookmarks:bookmarks];
        [self writeToFile];
        return;
    }
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    NSData *data = [[NSData alloc] initWithContentsOfFile: self.path];
    
    NSDictionary *saveData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    // NSInteger version = [[saveData objectForKey: @"version"] intValue];
    NSArray *bookmarks = [saveData objectForKey: @"bookmarks"];
    for(NSDictionary *d in bookmarks) {
        NSString *title = [d objectForKey: @"title"];
        NSString *url = [d objectForKey: @"url"];
        
        Bookmark *n = [[Bookmark alloc] initWithTitle:title url:url];
        [result addObject: n];
    }
    
    [self setBookmarks: result];
}

-(void)writeToFile {

    NSMutableArray *r = [[NSMutableArray alloc] init];
    for(Bookmark *bookmark in self.bookmarks){
        NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
        [d setObject:bookmark.title forKey:@"title"];
        [d setObject:bookmark.url forKey:@"url"];
        [r addObject: d];
    }
    
    NSMutableDictionary *n = [[NSMutableDictionary alloc] init];
    [n setObject:[NSNumber numberWithInteger:1] forKey:@"version"];
    [n setObject:r forKey:@"bookmarks"];
    
    NSString *directory = [self.path stringByDeletingLastPathComponent];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:directory]){
        [fileManager createDirectoryAtPath:directory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:n options:NSJSONWritingPrettyPrinted error:nil];
    [jsonData writeToFile:self.path atomically:YES];
    
}
@end
