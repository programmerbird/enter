//
//  FaviconFinder.m
//  Enter
//
//  Created by Sittipon Simasanti on 15/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "FaviconFinder.h"

@interface FaviconFinder() {
}
@property (nonatomic, strong) NSString *directory;
@property (nonatomic, strong) NSArray *applePatterns;
@property (nonatomic, strong) NSArray *faviconPatterns;
@property (nonatomic, strong) NSArray *linkPatterns;
@property (nonatomic, strong) NSRegularExpression *titlePattern;
@end
@implementation FaviconFinder

/*

<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

 <link href="http://someserver/favicon.ico" rel="shortcut icon" />
 <link href="http://someserver/favicon.ico" rel="shortcut" />
 <link href="http://someserver/favicon.ico" rel="icon" />
 <link href="http://someserver/favicon.png" rel="apple-touch-icon" />
 <link href="http://someserver/favicon.png" rel="apple-touch-icon-precomposed" />
 <link href="http://someserver/favicon.ico" rel="image_src" />
 <link rel="shortcut icon" href="https://static.xx.fbcdn.net/rsrc.php/yo/r/iRmz9lCMBD2.ico">
 <meta content="/images/branding/googleg/1x/googleg_standard_color_128dp.png" itemprop="image">
 

 "link[rel='icon']",
 "link[rel='shortcut icon']",
 "link[rel='apple-touch-icon']",
 "link[rel='apple-touch-icon-precomposed']",
 "link[rel='apple-touch-startup-image']",
 "link[rel='mask-icon']",
 "link[rel='fluid-icon']",
*/

-(id)init {
    self = [super init];
    if(self){
        NSRegularExpression *applePattern = [NSRegularExpression regularExpressionWithPattern:@"<link[^>]*rel=\"(apple-touch-icon|apple-touch-icon-precomposed)\"[^>]*>" options:NSRegularExpressionCaseInsensitive|NSRegularExpressionAnchorsMatchLines error:nil];
        NSArray *applePatterns = [[NSArray alloc] initWithObjects:applePattern, nil];
        [self setApplePatterns: applePatterns];

        NSRegularExpression *linkPattern = [NSRegularExpression regularExpressionWithPattern:@"<link[^>]*rel=\"(shortcut icon|icon|mask-icon|fluid-icon|image_src)\"[^>]*>" options:NSRegularExpressionCaseInsensitive|NSRegularExpressionAnchorsMatchLines error:nil];

        NSRegularExpression *metaPattern = [NSRegularExpression regularExpressionWithPattern:@"<meta[^>]*(itemprop=\"image\"|property=\"image\"|property=\"og:image\"|property=\"og:image:secure_url\"|name=\"twitter:image\")[^>]*>" options:NSRegularExpressionCaseInsensitive|NSRegularExpressionAnchorsMatchLines error:nil];
        NSArray *faviconPatterns = [[NSArray alloc] initWithObjects:linkPattern, metaPattern, nil];
        [self setFaviconPatterns: faviconPatterns];

        NSRegularExpression *href = [NSRegularExpression regularExpressionWithPattern:@"(href|content)=\"([^\"]*)\"" options:NSRegularExpressionCaseInsensitive|NSRegularExpressionAnchorsMatchLines error:nil];
        NSArray *linkPatterns = [[NSArray alloc] initWithObjects:href, nil];
        [self setLinkPatterns: linkPatterns];
        
        NSRegularExpression *titlePattern = [NSRegularExpression regularExpressionWithPattern:@"<title[^>]*>\\s*([^<]*)\\s*<" options:NSRegularExpressionCaseInsensitive|NSRegularExpressionAnchorsMatchLines error:nil];
        [self setTitlePattern: titlePattern];
    }
    return self;
    
}
-(id)initWithDirectory: (NSString *)directory{
    
    self = [self init];
    if(self){
        [self setDirectory: directory];
        
    }
    return self;
}

-(NSURL*)responseURLFromURL: (NSURL*)url {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15.0];
    [request setHTTPMethod:@"HEAD"];

    __block NSURL *resultURL = nil;
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        resultURL = response.URL;
        
        dispatch_semaphore_signal(semaphore);
    }];
    [task resume];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    return resultURL;
}

-(NSString*)htmlFromURL: (NSURL *)url {
    NSData *data = [NSData dataWithContentsOfURL: url];
    NSString *result;
    [NSString stringEncodingForData:data encodingOptions:nil convertedString:&result usedLossyConversion:nil];
    return result;
}


-(NSString*)titleFromHTML: (NSString*)html {
    NSRange range = NSMakeRange(0, html.length);
    NSTextCheckingResult *result = [self.titlePattern firstMatchInString:html options:0 range:range];
    if(!result){
        return nil;
    }
    NSRange matchRange = [result rangeAtIndex: 1];
    NSString *title = [html substringWithRange: matchRange];
    return [self textFromHTML: title];
}

-(NSString*)textFromHTML: (NSString*)html{
    return [[[[[html stringByReplacingOccurrencesOfString: @"&" withString: @"&amp;"]
    stringByReplacingOccurrencesOfString: @"\"" withString: @"&quot;"]
    stringByReplacingOccurrencesOfString: @"'" withString: @"&#39;"]
    stringByReplacingOccurrencesOfString: @">" withString: @"&gt;"]
    stringByReplacingOccurrencesOfString: @"<" withString: @"&lt;"];
}

-(NSString*)linkFromTag: (NSString*)tag {
    NSRange range = NSMakeRange(0, tag.length);
    
    for(NSRegularExpression *pattern in self.linkPatterns){
        NSTextCheckingResult *result = [pattern firstMatchInString:tag options:0 range:range];
        if(!result){
            continue;
        }
        NSRange matchRange = [result rangeAtIndex:2];
        return [tag substringWithRange: matchRange];
    }
    return nil;
}


-(NSArray *)appleIconURLsFromHTML: (NSString*)html relativeToURL: (NSURL*)url{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    if(!html){
        return result;
    }
    NSRange range = NSMakeRange(0, html.length);
    for(NSRegularExpression *pattern in self.applePatterns){
        NSArray *matches = [pattern matchesInString:html options:0 range:range];
        for(NSTextCheckingResult *match in matches){
            NSString *tag = [html substringWithRange:match.range];
            NSString *link = [self linkFromTag: tag];
            if(!link){
                continue;
            }
            NSURL *u = [NSURL URLWithString:link relativeToURL:url];
            if(!u){
                continue;
            }
            [result addObject: u];
        }
    }

    return result;
}

-(NSArray *)faviconURLsFromHTML: (NSString*)html relativeToURL: (NSURL*)url{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    if(!html){
        return result;
    }
    NSRange range = NSMakeRange(0, html.length);

    for(NSRegularExpression *pattern in self.faviconPatterns){
        NSArray *matches = [pattern matchesInString:html options:0 range:range];
        for(NSTextCheckingResult *match in matches){
            NSString *tag = [html substringWithRange:match.range];
            NSString *link = [self linkFromTag: tag];
            if(!link){
                continue;
            }
            NSURL *u = [NSURL URLWithString:link relativeToURL:url];
            if(!u){
                continue;
            }
            [result addObject: u];
        }
    }

    NSURL *atCurrentDir = [NSURL URLWithString:@"favicon.ico" relativeToURL:url];
    [result addObject: atCurrentDir];

    NSURL *atRootDir = [NSURL URLWithString:@"/favicon.ico" relativeToURL:url];
    [result addObject: atRootDir];
    return result;
}

-(void)removeImageCacheForURL: (NSURL*)url {
    NSString *host = url.host;
    NSString *root = [self.directory stringByAppendingPathComponent: host];

    NSFileManager *fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath: root]){
        return;
    }
    [fileManager removeItemAtPath:root  error:nil];
}

-(UIImage*)largestImageFromURLs:(NSArray*)imageURLs {
    CGFloat bestSize = 0;
    UIImage *bestImage = nil;
    for(NSURL *imageURL in imageURLs){
        NSData *data = [NSData dataWithContentsOfURL: imageURL];
        if(!data) {
            continue;
        }
        
        UIImage *image = [UIImage imageWithData: data];
        if(!bestImage || bestSize <= image.size.width) {
            bestImage = image;
            bestSize = image.size.width;
            continue;
        }
    }
    return bestImage;
}

-(UIImage*)paddedImage: (UIImage*)oldImage {
    
    CGFloat oldWidth = oldImage.size.width;
    CGFloat oldHeight = oldImage.size.height;
    
    CGFloat width = oldWidth * 1.25f;
    CGFloat height = oldHeight * 1.25f;
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, height), NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);

    // Now we can draw anything we want into this new context.
    CGPoint origin = CGPointMake((width - oldImage.size.width) / 2.0f,
                                (height - oldImage.size.height) / 2.0f);
    [oldImage drawAtPoint:origin];

    // Clean up and get the new image.
    UIGraphicsPopContext();
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage*)fallbackImageForURL: (NSURL*)url {
    NSURL *faviconKit = [NSURL URLWithString: [NSString stringWithFormat:@"https://api.faviconkit.com/%@/128", url.host]];
    NSData *data = [NSData dataWithContentsOfURL: faviconKit];
    if(!data){
        return nil;
    }
    return [UIImage imageWithData: data];
}

-(UIImage*)iconImageForURL: (NSURL*)url {
    NSString *host = url.host;
    NSString *root = [self.directory stringByAppendingPathComponent: host];
    NSString *filePath = [root stringByAppendingPathComponent: @"icon.png"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath: filePath]){
        UIImage *image = [UIImage imageWithContentsOfFile: filePath];
        if(image.size.width > 0){
            return image;
        }
    }
    
    if(![fileManager fileExistsAtPath: root]) {
        [fileManager createDirectoryAtPath:root withIntermediateDirectories:YES attributes:nil error:nil];
    }

    NSURL *responseURL = [self responseURLFromURL: url];
    if(!responseURL) {
        responseURL = url;
    }
    
    NSString *html = [self htmlFromURL: responseURL];
    if(!html){
        return nil;
    }
    
    NSArray *appleURLs = [self appleIconURLsFromHTML:html relativeToURL:responseURL];
    UIImage *bestImage = [self largestImageFromURLs: appleURLs];
    if(!bestImage) {
        NSArray *imageURLs = [self faviconURLsFromHTML:html relativeToURL:responseURL];
        bestImage = [self largestImageFromURLs: imageURLs];

        if(!bestImage || bestImage.size.width <= 64){
            UIImage *image = [self fallbackImageForURL: responseURL];
            if(image && bestImage.size.width < image.size.width){
                bestImage = image;
            }
        }
        
    }
    
    if(bestImage) {
        [UIImagePNGRepresentation(bestImage) writeToFile:filePath atomically:YES];
    }
    return bestImage;
}

@end
