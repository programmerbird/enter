//
//  SearchEngine.m
//  Enter
//
//  Created by Sittipon Simasanti on 17/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "SearchManager.h"

#define SEARCH_ENGINE @"searchEngine"

@implementation SearchManager

-(NSString*)templateForEngine: (nullable NSString*)engine {
    if(!engine) {
        NSUserDefaults *configs = [NSUserDefaults standardUserDefaults];
        engine = [configs objectForKey: SEARCH_ENGINE];
    }
    if([engine length] <= 0){
        return @"https://www.google.com/search?ie=UTF-8&oe=UTF-8&q=";
    }
    return engine;
}

-(NSURL*)urlForTerm: (NSString*)term engine: (nullable NSString*)engine {

    NSString *template = [self templateForEngine: engine];
    NSString *encodedTerm = [self urlEncodeTerm: term];
    if([template containsString: @"{{ query }}"]){
        NSString *x = [template stringByReplacingOccurrencesOfString:@"{{ query }}" withString:encodedTerm];
        return [NSURL URLWithString: x];
    }
    
    NSString *urlText = [NSString stringWithFormat: @"%@%@", template, encodedTerm];
    return [NSURL URLWithString:urlText];
}


-(NSString*)urlEncodeTerm: (NSString *)term {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[term UTF8String];
    NSInteger sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

@end
