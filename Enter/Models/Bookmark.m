//
//  Bookmark.m
//  Enter
//
//  Created by Sittipon Simasanti on 3/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "Bookmark.h"

@implementation Bookmark

-(id)initWithTitle: (NSString*)title url: (NSString*)url {
    self = [super init];
    if(self){
        [self setTitle: title];
        [self setUrl: url];
    }
    return self;
}

@end
