//
//  SearchEngine.h
//  Enter
//
//  Created by Sittipon Simasanti on 17/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchManager : NSObject

-(NSURL*)urlForTerm: (NSString*)term engine: (nullable NSString*)engine;
@end

NS_ASSUME_NONNULL_END
