//
//  AddToBookmarkActivity.h
//  Enter
//
//  Created by Sittipon Simasanti on 16/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddToBookmarkActivity : UIActivity

@property (nonatomic, weak) UIViewController *viewController;
@property (nonatomic, strong) NSString *webTitle;
@end

NS_ASSUME_NONNULL_END
