//
//  OpenInSafariActivity.h
//  Enter
//
//  Created by Sittipon Simasanti on 14/4/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenInSafariActivity : UIActivity

@end

NS_ASSUME_NONNULL_END
