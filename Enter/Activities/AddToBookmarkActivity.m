//
//  AddToBookmarkActivity.m
//  Enter
//
//  Created by Sittipon Simasanti on 16/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "AddToBookmarkActivity.h"
#import "BookmarkManager.h"
#import "Bookmark.h"
#import "FaviconFinder.h"

@interface AddToBookmarkActivity()

@property (nonatomic, strong) NSURL *activityURL;

@end
@implementation AddToBookmarkActivity

-(UIActivityType)activityType {
    return @"com.programmerbird.enter.addtobookmark";
}

-(NSString *)activityTitle {
    return NSLocalizedString(@"Add to Bookmark", nil);
}

-(UIImage *)activityImage {
    return [UIImage imageNamed: @"icon-bookmark"];
}

-(BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    for(NSObject *item in activityItems){
        if([item isKindOfClass:[NSURL class]]){
            return YES;
        }
    }
    return NO;
}

-(void)prepareWithActivityItems:(NSArray *)activityItems {
    for(NSObject *item in activityItems){
        if([item isKindOfClass:[NSURL class]]){
            self.activityURL = (NSURL*)item;
            return;
        }
    }
}

-(UIViewController *)activityViewController {
    return nil;
}

- (void)performActivity {
    
    if([self.webTitle length] > 0){
        [self askUserForTitle: self.webTitle];
        return;
    }
    
    NSURL *url = self.activityURL;
    __weak AddToBookmarkActivity *weakSelf = self;
    dispatch_async(BookmarkManager.queue, ^{
        FaviconFinder *finder = [[FaviconFinder alloc] init];
        NSString *html = [finder htmlFromURL: url];
        NSString *title = [finder titleFromHTML: html];
        if(title.length <= 0){
            title = @"";
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf askUserForTitle: title];
        });
    });
    
    
}


-(void)askUserForTitle: (NSString*)defaultTitle {
    
    __weak AddToBookmarkActivity *weakSelf = self;
    
    if(!self.viewController) {
        dispatch_async(BookmarkManager.queue, ^{
            [weakSelf writeBookmarkWithTitle: defaultTitle];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf activityDidFinish: YES];
            });
        });
        return;
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Add to Bookmark", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        [textField setPlaceholder: NSLocalizedString(@"Title", nil)];
        [textField setText: defaultTitle];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [alert dismissViewControllerAnimated:YES completion:^{
            [weakSelf activityDidFinish: NO];
        }];
    }];

    
    UIAlertAction *save = [UIAlertAction actionWithTitle:NSLocalizedString(@"Add", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UITextField *textField = [alert.textFields firstObject];
        NSString *userTitle = textField.text;
        
        dispatch_async(BookmarkManager.queue, ^{
            [weakSelf writeBookmarkWithTitle: userTitle];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf activityDidFinish: YES];
            });
        });
    }];

    [alert addAction: save];
    [alert addAction: cancel];

    [self.viewController presentViewController:alert animated:YES completion:^{
    }];
}




-(void)writeBookmarkWithTitle: (NSString*)title {
    
    Bookmark *b = [[Bookmark alloc] initWithTitle:title url:self.activityURL.absoluteString];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *bookmarkPath = [[paths firstObject] stringByAppendingPathComponent:@"Bookmarks.json"];
    BookmarkManager *manager = [[BookmarkManager alloc] initWithFile:bookmarkPath];

    [manager restoreFromFile];
    [manager.bookmarks addObject: b];
    [manager writeToFile];
    
}

@end
