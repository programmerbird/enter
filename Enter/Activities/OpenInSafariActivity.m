//
//  OpenInSafariActivity.m
//  Enter
//
//  Created by Sittipon Simasanti on 14/4/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "OpenInSafariActivity.h"

@interface OpenInSafariActivity()

@property (nonatomic, strong) NSURL *activityURL;

@end
@implementation OpenInSafariActivity

-(UIActivityType)activityType {
    return @"com.programmerbird.enter.openinsafari";
}

-(NSString *)activityTitle {
    return NSLocalizedString(@"Open in Safari", nil);
}

-(UIImage *)activityImage {
    return [UIImage imageNamed: @"icon-safari"];
}

-(BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    for(NSObject *item in activityItems){
        if([item isKindOfClass:[NSURL class]]){
            return YES;
        }
    }
    return NO;
}

-(void)prepareWithActivityItems:(NSArray *)activityItems {
    for(NSObject *item in activityItems){
        if([item isKindOfClass:[NSURL class]]){
            self.activityURL = (NSURL*)item;
            return;
        }
    }
}

-(UIViewController *)activityViewController {
    return nil;
}

- (void)performActivity {
    
    [[UIApplication sharedApplication] openURL:self.activityURL options:@{} completionHandler:^(BOOL success) {
    }];
}
@end
