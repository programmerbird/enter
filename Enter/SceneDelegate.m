#import "SceneDelegate.h"
#import "RootViewController.h"

@interface SceneDelegate ()

@end

@implementation SceneDelegate


- (void)scene:(UIScene *)scene willConnectToSession:(UISceneSession *)session options:(UISceneConnectionOptions *)connectionOptions  API_AVAILABLE(ios(13.0)){
    // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
    // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
    // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
    
    if(connectionOptions) {
        for(NSUserActivity *activity in connectionOptions.userActivities){
            [self runUserActivity: activity];
            return;
        }
        
        for(UIOpenURLContext *context in connectionOptions.URLContexts){
            if(!context.URL){
                continue;
            }
            NSURL *url = context.URL;
            [self runURL: url];
            return;
        }

    }

    
}


- (void)sceneDidDisconnect:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    // Called as the scene is being released by the system.
    // This occurs shortly after the scene enters the background, or when its session is discarded.
    // Release any resources associated with this scene that can be re-created the next time the scene connects.
    // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
}


- (void)sceneDidBecomeActive:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    // Called when the scene has moved from an inactive state to an active state.
    // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
}


- (void)sceneWillResignActive:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    // Called when the scene will move from an active state to an inactive state.
    // This may occur due to temporary interruptions (ex. an incoming phone call).
}


- (void)sceneWillEnterForeground:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    // Called as the scene transitions from the background to the foreground.
    // Use this method to undo the changes made on entering the background.
}


- (void)sceneDidEnterBackground:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    // Called as the scene transitions from the foreground to the background.
    // Use this method to save data, release shared resources, and store enough scene-specific state information
    // to restore the scene back to its current state.
}


-(void)scene:(UIScene *)scene openURLContexts:(NSSet<UIOpenURLContext *> *)URLContexts  API_AVAILABLE(ios(13.0)){
    
    if (@available(iOS 13.0, *)) {
        for(UIOpenURLContext *context in URLContexts){
            if(!context.URL){
                continue;
            }
            NSURL *url = context.URL;
            [self runURL: url];
        }
    } else {
        // Fallback on earlier versions
    }
}


-(void)runURL: (NSURL*)url {
    
    NSString *scheme = url.scheme;
    if([scheme isEqualToString: @"programmerbird-enters"]){
        NSURL *webURL = [NSURL URLWithString:[url.absoluteString stringByReplacingOccurrencesOfString:@"programmerbird-enters://" withString:@"https://"]];
        [self openURL: webURL];
        return;
    }
    else if([scheme isEqualToString: @"programmerbird-enter"]){
        NSURL *webURL = [NSURL URLWithString:[url.absoluteString stringByReplacingOccurrencesOfString:@"programmerbird-enter://" withString:@"http://"]];
        [self openURL: webURL];
        return;
    }
    else if([scheme isEqualToString: @"https"]) {
        [self openURL: url];
        return;
    }
    else if([scheme isEqualToString: @"http"]) {
        [self openURL: url];
        return;
    }
}

-(BOOL)openURL: (NSURL *)url {
    RootViewController *root = (RootViewController*)self.window.rootViewController;
    [root onReady:^{
        [root loadWeb: url];
    }];
    return NO;
}

-(void)runUserActivity: (NSUserActivity*)userActivity {
    NSString *activityType = [userActivity activityType];
    if([activityType isEqualToString: @"com.programmerbird.Enter.OpenURL"]){
        NSURL *url = [userActivity.userInfo objectForKey: @"url"];
        RootViewController *root = (RootViewController*)self.window.rootViewController;
        [root onReady:^{
            [root loadWeb: url];
        }];
        return;
    }
    if([activityType isEqualToString: @"com.programmerbird.Enter.SearchWeb"]){
        NSString *text = [userActivity.userInfo objectForKey: @"text"];
        NSString *service = [userActivity.userInfo objectForKey: @"service"];

        RootViewController *root = (RootViewController*)self.window.rootViewController;
        [root onReady:^{
            [root loadSearch:text engine:service];
        }];
        return;
    }
    
}

-(void)scene:(UIScene *)scene continueUserActivity:(NSUserActivity *)userActivity  API_AVAILABLE(ios(13.0)){
    
    [self runUserActivity: userActivity];

}

@end
