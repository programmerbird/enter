//
//  BookmarkCell.h
//  Enter
//
//  Created by Sittipon Simasanti on 3/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BookmarkCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *iconView;
@property (nonatomic, weak) IBOutlet UILabel *label;
@property (nonatomic, weak) IBOutlet UILabel *letterLabel;

@end

NS_ASSUME_NONNULL_END
