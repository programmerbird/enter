//
//  RichLabel.h
//  Enter
//
//  Created by Sittipon Simasanti on 1/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RichLabel : UIView

@property (nonatomic, weak) IBOutlet UIView *searchImage;
@property (nonatomic, weak) IBOutlet UIView *lockImage;
@property (nonatomic, weak) IBOutlet UILabel *prefixLabel;
@property (nonatomic, weak) IBOutlet UILabel *mainLabel;
@property (nonatomic, weak) IBOutlet UILabel *postfixLabel;

@property (assign) BOOL isSearch;
@property (assign) BOOL isLock;
@property (nonatomic, strong, nullable) NSString *prefixText;
@property (nonatomic, strong, nullable) NSString *mainText;
@property (nonatomic, strong, nullable) NSString *postfixText;

-(void)reloadLayout;

@end

NS_ASSUME_NONNULL_END
