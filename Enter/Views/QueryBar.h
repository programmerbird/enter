//
//  QueryBar.h
//  Enter
//
//  Created by Sittipon Simasanti on 1/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol QueryBarDelegate <NSObject>
-(void)queryBar: (id)sender willShowTextField: (UITextField *)textField;
-(void)queryBar: (id)sender willDismissTextField: (UITextField *)textField;
@end
@interface QueryBar : UIView<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIButton *scanButton;
@property (nonatomic, weak) IBOutlet UIButton *reloadButton;
@property (nonatomic, weak) IBOutlet UIButton *titleButton;
@property (nonatomic, weak) IBOutlet UIView *titleLabel;

@property (nonatomic, weak) IBOutlet UIButton *cancelButton;
@property (nonatomic, weak) IBOutlet UITextField *textField;

@property (nonatomic, weak) IBOutlet id<QueryBarDelegate> delegate;


-(IBAction)titleDidTap: (id)sender;
-(IBAction)cancelDidTap: (id)sender;

-(void)showTextFieldAnimated: (BOOL)animated;
-(void)dismissTextFieldAnimated: (BOOL)animated;
@end

NS_ASSUME_NONNULL_END
