//
//  RichLabel.m
//  Enter
//
//  Created by Sittipon Simasanti on 1/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "RichLabel.h"

#define ICON_SIZE  44
#define ICON_PADDING -12

@implementation RichLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)reloadLayout {
    CGFloat cursor = 0;
    
    CGFloat vh = self.bounds.size.height;
    
    if(!_isSearch){
        [self.searchImage setHidden: YES];
    }
    else{
        cursor += ICON_PADDING;
        [self.searchImage setFrame: CGRectMake(cursor, 0, ICON_SIZE, vh)];
        [self.searchImage setHidden: NO];
        cursor += ICON_SIZE;
        cursor += ICON_PADDING;
    }

    if(!_isLock){
        [self.lockImage setHidden: YES];
    }
    else {
        cursor += ICON_PADDING;
        [self.lockImage setFrame: CGRectMake(cursor, 0, ICON_SIZE, vh)];
        [self.lockImage setHidden: NO];
        cursor += ICON_SIZE;
        cursor += ICON_PADDING;
    }
    
    BOOL hasPrefix = [self.prefixText length] > 0;
    BOOL hasMain = [self.mainText length] > 0;
    BOOL hasPostfix = [self.postfixText length] > 0;
    
    if(!hasPrefix) {
        [self.prefixLabel setHidden: YES];
    }
    else {
        [self.prefixLabel setText: self.prefixText];
        [self.prefixLabel sizeToFit];
        
        CGFloat w = self.prefixLabel.frame.size.width;
        [self.prefixLabel setFrame: CGRectMake(cursor, 0, w, vh)];
        [self.prefixLabel setHidden: NO];
        cursor += w;
    }

    if(!hasMain) {
        [self.mainLabel setHidden: YES];
    }
    else {
        [self.mainLabel setText: self.mainText];
        [self.mainLabel sizeToFit];
        
        CGFloat w = self.mainLabel.frame.size.width;
        [self.mainLabel setFrame: CGRectMake(cursor, 0, w, vh)];
        [self.mainLabel setHidden: NO];
        cursor += w;
    }

    if(!hasPostfix) {
        [self.postfixLabel setHidden: YES];
    }
    else {
        [self.postfixLabel setText: self.postfixText];
        [self.postfixLabel sizeToFit];
        
        CGFloat w = self.postfixLabel.frame.size.width;
        [self.postfixLabel setFrame: CGRectMake(cursor, 0, w, vh)];
        [self.postfixLabel setHidden: NO];
        cursor += w;
    }

    CGPoint offset = self.frame.origin;
    [self setFrame: CGRectMake(offset.x, offset.y, cursor, vh)];
}

@end
