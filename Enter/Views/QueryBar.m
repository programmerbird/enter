//
//  QueryBar.m
//  Enter
//
//  Created by Sittipon Simasanti on 1/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "QueryBar.h"

#define TEXT_MARGIN         10
#define ANIMATE_DURATION    0.3f

typedef NS_ENUM(NSInteger, QueryState){
    QueryStateButtons,
    QueryStateTextField,
};

@implementation QueryBar {
    QueryState _state;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder: coder];
    _state = QueryStateButtons;
    return self;
}

-(void)showTextFieldAnimated: (BOOL)animated {
    if(_state != QueryStateTextField){
        [self.delegate queryBar: self willShowTextField: self.textField];
    }

    CGFloat vw = self.bounds.size.width;
    CGFloat vh = self.bounds.size.height;
    
    if(!animated){
        _state = QueryStateTextField;
        
        CGFloat cancelWidth = self.cancelButton.frame.size.width;
        [self.scanButton setHidden: YES];
        [self.reloadButton setHidden: YES];
        [self.titleButton setHidden: YES];
        [self.titleLabel setHidden: YES];
        [self.cancelButton setFrame: CGRectMake(vw - cancelWidth, 0, cancelWidth, vh)];
        [self.cancelButton setAlpha: 1];
        [self.cancelButton setHidden: NO];

        [self.textField setFrame: CGRectMake(TEXT_MARGIN, 0, vw - cancelWidth - TEXT_MARGIN, vh)];
        [self.textField setAlpha: 1];
        [self.textField setHidden: NO];
        return;
    }
    if(_state == QueryStateTextField){
        return;
    }
    
    _state = QueryStateTextField;
    
    CGFloat cancelWidth = self.cancelButton.frame.size.width;
    CGFloat scanWidth = self.scanButton.frame.size.width;
    CGFloat moreWidth = self.reloadButton.frame.size.width;
    CGFloat titleWidth = self.titleLabel.frame.size.width;
    
    [self.titleButton setHidden: YES];
    [self.cancelButton setFrame: CGRectMake(vw, 0, scanWidth, vh)];
    [self.cancelButton setAlpha: 0];
    [self.cancelButton setHidden: NO];
    
    [self.textField setFrame: CGRectMake(self.titleLabel.frame.origin.x, 0, vw - TEXT_MARGIN - TEXT_MARGIN, vh)];
    [self.textField setAlpha: 0];
    [self.textField setHidden: NO];
    
    __weak QueryBar *weakSelf = self;
    [UIView animateWithDuration:ANIMATE_DURATION delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [weakSelf.titleLabel setFrame: CGRectMake(TEXT_MARGIN, 0, titleWidth, vh)];
        [weakSelf.titleLabel setAlpha: 0];
        
        [weakSelf.scanButton setFrame: CGRectMake(-scanWidth, 0, scanWidth, vh)];
        [weakSelf.scanButton setAlpha: 0];
        
        [weakSelf.reloadButton setFrame: CGRectMake(vw, 0, moreWidth, vh)];
        [weakSelf.reloadButton setAlpha: 0];
        
        [weakSelf.cancelButton setFrame: CGRectMake(vw - cancelWidth, 0, cancelWidth, vh)];
        [weakSelf.cancelButton setAlpha: 1];
        
        [weakSelf.textField setFrame: CGRectMake(TEXT_MARGIN, 0, vw - cancelWidth - TEXT_MARGIN, vh)];
        [weakSelf.textField setAlpha: 1];
    } completion:^(BOOL finished) {
        if(![weakSelf.titleButton isHidden]) {
            return;
        }
        [weakSelf showTextFieldAnimated: NO];
    }];
}
-(void)dismissTextFieldAnimated: (BOOL)animated {
    if(_state != QueryStateButtons){
        [self.delegate queryBar: self willDismissTextField: self.textField];
    }

    CGFloat vw = self.bounds.size.width;
    CGFloat vh = self.bounds.size.height;

    CGFloat titleWidth = self.titleLabel.frame.size.width;
    CGFloat tw = self.titleLabel.superview.bounds.size.width;
    CGFloat tl = (tw-titleWidth)/2.0f;
    if(tl < 0){
        tl = 0;
    }

    if(!animated){
        _state = QueryStateButtons;
        [self.cancelButton setHidden: YES];
        [self.textField setHidden: YES];
        
        [self.titleLabel setFrame: CGRectMake(tl, 0, titleWidth, vh)];
        [self.titleLabel setAlpha: 1];
        [self.titleLabel setHidden: NO];
        
        CGFloat scanWidth = self.scanButton.frame.size.width;
        [self.scanButton setFrame: CGRectMake(0, 0, scanWidth, vh)];
        [self.scanButton setAlpha: 1];
        [self.scanButton setHidden: NO];
        
        CGFloat moreWidth = self.reloadButton.frame.size.width;
        [self.reloadButton setFrame: CGRectMake(vw - moreWidth, 0, moreWidth, vh)];
        [self.reloadButton setAlpha: 1];
        [self.reloadButton setHidden: NO];
        
        [self.titleButton setHidden: NO];
        return;
    }
    
    if(_state == QueryStateButtons){
        return;
    }
    _state = QueryStateButtons;
    [self.titleButton setHidden: NO];
    
    [self.titleLabel setFrame: CGRectMake(0, 0, titleWidth, vh)];
    [self.titleLabel setAlpha: 0];
    [self.titleLabel setHidden: NO];
    
    CGFloat scanWidth = self.scanButton.frame.size.width;
    [self.scanButton setFrame: CGRectMake(-scanWidth, 0, scanWidth, vh)];
    [self.scanButton setAlpha: 0];
    [self.scanButton setHidden: NO];
    
    CGFloat moreWidth = self.reloadButton.frame.size.width;
    [self.reloadButton setFrame: CGRectMake(vw, 0, moreWidth, vh)];
    [self.reloadButton setAlpha: 0];
    [self.reloadButton setHidden: NO];
    
    __weak QueryBar *weakSelf = self;
    
    CGFloat cancelWidth = self.cancelButton.frame.size.width;
    [UIView animateWithDuration:ANIMATE_DURATION delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        [weakSelf.titleLabel setFrame: CGRectMake(tl, 0, titleWidth, vh)];
        [weakSelf.titleLabel setAlpha: 1];
        
        [weakSelf.scanButton setFrame: CGRectMake(0, 0, scanWidth, vh)];
        [weakSelf.scanButton setAlpha: 1];
        [weakSelf.reloadButton setFrame: CGRectMake(vw-moreWidth, 0, moreWidth, vh)];
        [weakSelf.reloadButton setAlpha: 1];
        [weakSelf.cancelButton setFrame: CGRectMake(vw, 0, cancelWidth, vh)];
        [weakSelf.cancelButton setAlpha: 0];
        [weakSelf.textField setFrame: CGRectMake(TEXT_MARGIN, 0, vw - TEXT_MARGIN - TEXT_MARGIN, vh)];
        [weakSelf.textField setAlpha: 0];
        
    } completion:^(BOOL finished) {
        if([weakSelf.titleButton isHidden]) {
            return;
        }
        [weakSelf dismissTextFieldAnimated: NO];
    }];
}

-(IBAction)titleDidTap: (id)sender {
    [self showTextFieldAnimated: YES];
    [self.textField becomeFirstResponder];
}

-(IBAction)cancelDidTap: (id)sender {
    [self.textField resignFirstResponder];
    [self dismissTextFieldAnimated: YES];
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    dispatch_async(dispatch_get_main_queue(), ^{
        [textField selectAll: nil];
        // [textField setTintColor: [UIColor clearColor]];
    });
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    [self.textField setTintColor: self.tintColor];
}

-(IBAction)textFieldDidChange: (id)sender {
    [self.textField setTintColor: self.tintColor];
}
-(IBAction)textFieldDidTouchDown: (id)sender {
    [self.textField setTintColor: self.tintColor];
}

-(void)textFieldDidChangeSelection:(UITextField *)textField {
    [self.textField setTintColor: self.tintColor];
}


@end

