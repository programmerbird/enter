//
//  FullScreenWebView.m
//  Enter
//
//  Created by Sittipon Simasanti on 1/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "FullScreenWebView.h"

#define SAFE_TOP 100
#define SAFE_BOTTOM 100

@implementation FullScreenWebView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(UIEdgeInsets)safeAreaInsets {
    UIEdgeInsets insets = [super safeAreaInsets];
    return UIEdgeInsetsMake(SAFE_TOP, insets.left, SAFE_BOTTOM, insets.right);
}

-(void)setVisibleFrame: (CGRect)frame {
    
    /*
    if(self.scrollView.contentInset.top != SAFE_TOP ||
       self.scrollView.contentInset.bottom != SAFE_BOTTOM)
    {
        [self.scrollView setContentInset: UIEdgeInsetsMake(SAFE_TOP, 0, SAFE_BOTTOM, 0)];
        [self.scrollView setScrollIndicatorInsets: UIEdgeInsetsMake(SAFE_TOP, 0, SAFE_BOTTOM, 0)];
    }
    */
    [self setFrame: CGRectMake(frame.origin.x, frame.origin.y - SAFE_TOP, frame.size.width, frame.size.height + SAFE_TOP + SAFE_BOTTOM)];
}
@end
