//
//  FakeProgressView.m
//  PlasticPod
//
//  Created by Sittipon Simasanti on 5/29/15.
//  Copyright (c) 2015 Apisith Interplast. All rights reserved.
//

#import "FakeProgressView.h"

@interface FakeProgressView () {

    BOOL _finished;
}

@property (nonatomic, strong) NSTimer *timer;

@end
@implementation FakeProgressView

-(void)startLoading {
    
    _finished = NO;
    [self setProgress: 0];
    [self setHidden: NO];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.015 target:self selector:@selector(tick) userInfo:nil repeats:YES];
    [self setTimer: timer];
}

-(void)stopLoading {
    _finished = YES;
    [self setHidden: YES];
}

-(void)tick {
    
    if (_finished){
        if(self.progress >= 1){
            [self setHidden: YES];
            [self.timer invalidate];
            [self setTimer: nil];
        }
        else{
            
            CGFloat p = self.progress;
            p += 0.1f;
            [self setProgress: p];
        }
    }
    else{
        
        CGFloat p = self.progress;
        if(p >= 0.9f){
            p += 0.000125f;
        }
        else if(p >= 0.8f){
            p += 0.00125f;
        }
        else if(p >= 0.5f){
            p += 0.0025f;
        }
        else if(p >= 0.25f){
            p += 0.005f;
        }
        else{
            p += 0.01f;
        }
        
        if(p >= 0.95f){
            p = 0.95f;
        }
        if([self.delegate fakeProgressCanEstimate: self]){
            CGFloat max = [self.delegate fakeProgressEstimateProgress: self];
            if(p > max){
                p = max;
            }
        }
        
        [self setProgress: p];
    }
}

@end
