//
//  FullScreenWebView.h
//  Enter
//
//  Created by Sittipon Simasanti on 1/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FullScreenWebView : WKWebView

-(void)setVisibleFrame: (CGRect)frame;

@end

NS_ASSUME_NONNULL_END
