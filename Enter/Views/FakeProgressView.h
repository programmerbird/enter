//
//  FakeProgressView.h
//  PlasticPod
//
//  Created by Sittipon Simasanti on 5/29/15.
//  Copyright (c) 2015 Apisith Interplast. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FakeProgressViewDelegate <NSObject>

-(BOOL)fakeProgressCanEstimate: (id)sender;
-(CGFloat)fakeProgressEstimateProgress: (id)sender;

@end
@interface FakeProgressView : UIProgressView

@property (nonatomic, weak) IBOutlet id<FakeProgressViewDelegate>delegate;

-(void)startLoading;
-(void)stopLoading;
@end
