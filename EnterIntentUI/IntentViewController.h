//
//  IntentViewController.h
//  EnterIntentUI
//
//  Created by Sittipon Simasanti on 17/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <IntentsUI/IntentsUI.h>

@interface IntentViewController : UIViewController <INUIHostedViewControlling>

@end
