//
//  FaviconFinderTests.m
//  EnterTests
//
//  Created by Sittipon Simasanti on 15/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "../Enter/Models/FaviconFinder.h"

@interface FaviconFinderTests : XCTestCase

@property (nonatomic, strong) FaviconFinder *finder;
@end

@implementation FaviconFinderTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dir = [[paths firstObject] stringByAppendingPathComponent: @"favicon"];
    FaviconFinder *finder = [[FaviconFinder alloc] initWithDirectory: dir];
    [self setFinder: finder];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testResponseURLFromURL {
    NSURL *url = [NSURL URLWithString: @"https://wikipedia.org"];
    NSURL *result = [self.finder responseURLFromURL: url];
    XCTAssertEqualObjects(result.absoluteString, @"https://www.wikipedia.org/");
}

-(void)testHTMLFromURL {
    NSURL *url = [NSURL URLWithString: @"https://www.wikipedia.org"];
    NSString *html = [self.finder htmlFromURL: url];
    XCTAssertTrue([html containsString:@"<body"]);
}

-(void)testLinkFromTag {
    NSString *href;
    href = [self.finder linkFromTag: @"<link rel=\"apple-touch-icon\" href=\"/somewhere/to/favicon.png\">"];
    XCTAssertEqualObjects(href, @"/somewhere/to/favicon.png");
    
    href = [self.finder linkFromTag: @"<link REL=\"shortcut icon\" HREF=\"./favicon.ico\">"];
    XCTAssertEqualObjects(href, @"./favicon.ico");
    
    
    href = [self.finder linkFromTag:@"<meta content=\"/images/branding/googleg/1x/googleg_standard_color_128dp.png\" itemprop=\"image\">"];
    XCTAssertEqualObjects(href, @"/images/branding/googleg/1x/googleg_standard_color_128dp.png");
}

-(void)testDownloadIconPathFromGoogle {
    
    NSURL *url = [NSURL URLWithString: @"https://www.google.com"];
    [self.finder removeImageCacheForURL: url];

    NSString *html = [self.finder htmlFromURL: url];
    NSArray *result = [self.finder faviconURLsFromHTML:html relativeToURL:url];
    XCTAssertTrue([result count] > 0);
    
    
    UIImage *image = [self.finder iconImageForURL: url];
    XCTAssertTrue(image.size.width >= 128);


}
-(void)testTitleFromStackOverflow {
    NSURL *url = [NSURL URLWithString: @"https://stackoverflow.com"];
    NSString *html = [self.finder htmlFromURL: url];
    NSString *title = [self.finder titleFromHTML: html];
    
    NSLog(@"title: %@", title);
    
    XCTAssertTrue([title containsString: @"Stack Overflow"]);

    
}
-(void)testTitleFromBlognone {
    NSURL *url = [NSURL URLWithString: @"https://www.blognone.com"];
    NSString *html = [self.finder htmlFromURL: url];
    NSString *title = [self.finder titleFromHTML: html];
    
    NSLog(@"title: %@", title);
    
    XCTAssertTrue([title containsString: @"Blognone"]);

    
}
-(void)testDownloadIconPathFromStackOverflow {
    NSURL *url = [NSURL URLWithString: @"https://stackoverflow.com"];
    [self.finder removeImageCacheForURL: url];

    NSString *html = [self.finder htmlFromURL: url];
    NSArray *result = [self.finder appleIconURLsFromHTML:html relativeToURL:url];
    XCTAssertTrue([result count] > 0);
    

    UIImage *image = [self.finder iconImageForURL: url];
    XCTAssertTrue(image.size.width >= 128);

}

-(void)testLinkFromTagFacebook{
    NSURL *url = [NSURL URLWithString:@"https://www.facebook.com/"];
    NSArray *result = [self.finder faviconURLsFromHTML:@"<meta property=\"al:ios:url\" content=\"fb://feed/\" /><link rel=\"shortcut icon\"\nhref=\"https://static.xx.fbcdn.net/rsrc.php/yz/r/KFyVIAWzntM.ico\" /><script>" relativeToURL:url];
    
    
    NSURL *r = [result firstObject];
    XCTAssertEqualObjects(r.absoluteString, @"https://static.xx.fbcdn.net/rsrc.php/yz/r/KFyVIAWzntM.ico");

    
}

-(void)testDownloadIconPathFromFacebook {
    
    NSURL *url = [NSURL URLWithString: @"https://www.facebook.com"];
    [self.finder removeImageCacheForURL: url];

    UIImage *image = [self.finder iconImageForURL: url];
    XCTAssertTrue(image.size.width >= 32);

}


@end
