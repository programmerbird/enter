//
//  IntentHandler.m
//  EnterIntent
//
//  Created by Sittipon Simasanti on 17/3/2563 BE.
//  Copyright © 2563 Sittipon Simasanti. All rights reserved.
//

#import "IntentHandler.h"
#import "OpenURLIntent.h"
#import "SearchWebIntent.h"
#import "SoundManager.h"

// The intents you wish to handle must be declared in the extension's Info.plist.

@interface IntentHandler ()
@end

@implementation IntentHandler
@end

@interface IntentHandler(OpenURL)<OpenURLIntentHandling>
@end
@implementation IntentHandler(OpenURL)

- (void)handleOpenURL:(OpenURLIntent *)intent completion:(void (^)(OpenURLIntentResponse *response))completion {
    
    NSUserActivity *activity = [[NSUserActivity alloc] initWithActivityType: @"com.programmerbird.Enter.OpenURL"];
    [activity setUserInfo: @{@"url": intent.url}];
    OpenURLIntentResponse *response = [[OpenURLIntentResponse alloc] initWithCode:OpenURLIntentResponseCodeContinueInApp userActivity:activity];
    completion(response);

}


-(void)resolveUrlForOpenURL:(OpenURLIntent *)intent withCompletion:(void (^)(INURLResolutionResult *resolutionResult))completion NS_AVAILABLE_IOS(13.0){
    if(!intent.url) {
        completion([INURLResolutionResult needsValue]);
        return;
    }
    completion([INURLResolutionResult successWithResolvedURL: intent.url]);
}
@end


@interface IntentHandler(SearchWeb)<SearchWebIntentHandling>
@end
@implementation IntentHandler(SearchWeb)



-(NSString *)templateForService: (Service)service {
    switch (service) {
        case ServiceUnknown:
            return @"";
        case ServiceBing:
            return @"https://www.bing.com/search?q=";
        case ServiceAmazon:
            return @"https://www.amazon.com/s?k=";
        case ServiceDuckduckgo:
            return @"https://duckduckgo.com/?q=";
        case ServiceEbay:
            return @"https://www.ebay.com/sch/i.html?_nkw=";
        case ServiceGoogle:
            return @"https://www.google.com/search?q=";
        case ServiceReddit:
            return @"https://www.reddit.com/search/?q=";
        case ServiceTwitter:
            return @"https://twitter.com/search?q=";
        case ServiceYahoo:
            return @"https://search.yahoo.com/search?p=";
        case ServiceYoutube:
            return @"https://www.youtube.com/results?search_query=";
    }
    return @"";
}
- (void)handleSearchWeb:(nonnull SearchWebIntent *)intent completion:(nonnull void (^)(SearchWebIntentResponse * _Nonnull))completion {
 
    
    NSUserActivity *activity = [[NSUserActivity alloc] initWithActivityType: @"com.programmerbird.Enter.SearchWeb"];
    NSString *service = [self templateForService: intent.service];
    [activity setUserInfo: @{
        @"service": service,
        @"text": intent.text,
    }];
    SearchWebIntentResponse *response = [[SearchWebIntentResponse alloc] initWithCode:SearchWebIntentResponseCodeContinueInApp userActivity:activity];
    completion(response);
}

- (void)resolveServiceForSearchWeb:(nonnull SearchWebIntent *)intent withCompletion:(nonnull void (^)(ServiceResolutionResult * _Nonnull))completion NS_AVAILABLE_IOS(13.0){
    if(intent.service == ServiceUnknown){
        completion([ServiceResolutionResult notRequired]);
        return;
    }
    completion([ServiceResolutionResult successWithResolvedService: intent.service]);
}

- (void)resolveTextForSearchWeb:(nonnull SearchWebIntent *)intent withCompletion:(nonnull void (^)(INStringResolutionResult * _Nonnull))completion {
    if([intent.text length] <= 0){
        completion([INStringResolutionResult needsValue]);
        return;
    }
    completion([INStringResolutionResult successWithResolvedString: intent.text]);
}

@end

