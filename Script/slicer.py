#
#  genwheel.py
#  Colors
#
#  Created by Bird on 6/27/09.
#  Copyright (c) 2009 Extend Interactive. All rights reserved.
#


import sys 
from PIL import Image, ImageOps
from utils import image, parameters, hex2color


def mask_picture(picture, mask):
	im = image(picture)
	mask = image(mask)

	im = ImageOps.fit(im, mask.size, Image.ANTIALIAS, 0, (0.5, 0.5))	

	(w, h) = mask.size

	mm = mask.load()
	ss = im.load()

	data = []
	blank = (0, 0, 0, 0)
	for y in range(h):
		for x in range(w):
			opacity = mm[x,y][0]
			sr,sg,sb,sa = ss[x, y]

			if opacity == 0:
				color = blank
			else:
				color = (sr,sg,sb,opacity)
			data.append(color)

	output = Image.new('RGBA', mask.size, blank)
	output.putdata(data)
	return output

def get_size(params):
	rotate = params.get('rotate') or False
	if isinstance(rotate, str):
		if rotate.lower() in ('n', 'f', 'false', '0', 'no'):
			rotate = False
	template = params.get('template')
	w = 0
	h = 0
	if template:
		template = image(template)
		(w, h) = template.size
	if params.get('size'):
		w = h = int(params.get('size'))
	if params.get('width'):
		w = int(params.get('width'))
	if params.get('height'):
		h = int(params.get('height'))
	return dict(size=(w,h), rotate=rotate)


def resize_aspect_fill(picture, size, rotate=False):
	(tw, th) = size

	picture = image(picture)
	(pw, ph) = picture.size

	if rotate:
		picture_is_horizontal = (pw >= ph)
		template_is_horizontal = (tw >= th)
		if picture_is_horizontal != template_is_horizontal:
			size = (th, tw)

	picture = ImageOps.fit(picture, size, Image.ANTIALIAS, 0, (0.5, 0.5))	
	return picture

def resize_aspect_fit(picture, size, rotate=False):
	picture = image(picture)
	pw, ph = picture.size
	tw, th = size

	if rotate:
		picture_is_horizontal = (pw >= ph)
		template_is_horizontal = (tw >= th)
		if picture_is_horizontal != template_is_horizontal:
			size = (th, tw)
			tw, th = size

	picture.thumbnail(size, Image.ANTIALIAS)
	return picture



def screenshot(picture):
	picture = image(picture)
	w, h = picture.size
	return picture.crop((0, 40, w, h))


def unretina(filename):
	im = image(filename)
	(w, h) = im.size
	return im.resize((int(w/2),int(h/2)), Image.ANTIALIAS)
	# return ImageOps.fit(im, (w/2,h/2), Image.ANTIALIAS, 0, (0.5, 0.5))		


def main():
	args, params = parameters()
	action = args[1]
	output = args[2]
	if action == 'mask':
		im = mask_picture(**params)
		im.save(output, "PNG")

	elif action == 'aspectfit':
		picture = params.get('picture')
		picture = resize_aspect_fit(picture, **get_size(params))
		picture.save(output)

	elif action == 'aspectfill':
		picture = params.get('picture')
		picture = resize_aspect_fill(picture, **get_size(params))
		picture.save(output)

	elif action == 'unretina':
		picture = params.get('picture')
		picture = unretina(picture)
		picture.save(output)

	elif action == 'screenshot':
		picture = params.get('picture')
		picture = screenshot(picture)
		picture.save(output)

	elif action == 'color':
		picture = Image.new('RGBA', get_size(params)['size'], color=hex2color(params['color']))
		picture.save(output)

	
if __name__ == '__main__':
	main()