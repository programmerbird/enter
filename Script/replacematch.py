
import os
import shutil


def find_file(filename, subdirectory=''):
    if subdirectory:
        path = subdirectory
    else:
        path = os.getcwd()
    for root, dirs, names in os.walk(path):
        if 'Resources' in root:
            continue
        if filename in names:
            yield os.path.join(root, filename)
        
def replace_file(project, filename):
    basename = os.path.basename(filename)
    files = list(find_file(basename, project))
    if len(files) == 1:
        projectfile = files[0]
        shutil.copy2(filename, projectfile)
        print(projectfile)

def main():
    import argparse

    parser = argparse.ArgumentParser(description='Find and replace find.')
    parser.add_argument('--project', type=str)
    parser.add_argument('filename', type=str)

    args = parser.parse_args()


    replace_file(args.project, args.filename)

if __name__ == '__main__':
    main()