#!/usr/bin/env 

import sys
import json

def pipe(*methods):
	def wrapper(*params):
		result = params
		for method in methods:
			result = method(*result)
		return result
	return wrapper


def parameters():
	args = []
	kwargs = {}
	key = None
	for x in sys.argv:
		if key:
			kwargs[key] = x 
			key = None
		elif x.startswith('--'):
			key = x[2:]
		else:
			args.append(x)
	return args, kwargs 

def round_color(r,g,b):
	return (int(r), int(g), int(b))

HEX = '0123456789abcdef'
HEXDICT = dict([(x, i) for (i, x) in enumerate(HEX)])

def hex(v):
	v = int(v)
	return ('%s%s' % (HEX[v / 16], HEX[v % 16])).upper()

def hex2color(hexcode):
    m = [ HEXDICT[h] for h in hexcode.lower() ]
    l = len(m)
    if l == 3:
        return (m[0] * 17, m[1] * 17, m[2] * 17)
    elif l == 6:
        return (m[0]*16 + m[1], m[2]*16 + m[3], m[4]*16 + m[5])
    elif l == 8:
        return (m[0]*16 + m[1], m[2]*16 + m[3], m[4]*16 + m[5], m[6]*16 + m[7])


def clamp_angle(a):
	while a < 0:
		a += 360.0
	while a >= 360.0:
		a -= 360.0
	return a

def invert_image(im):
    w, h = im.size
    pixels = im.load()
    for x in range(w):
        for y in range(h):
            (r,g,b,a) = pixels[x, y]
            pixels[x, y] = (255-r,255-g,255-b, a)
    return im

def image(filename_or_image, colorspace='RGBA'):
	from PIL import Image
	if isinstance(filename_or_image, str):

		invert = False
		if filename_or_image.startswith('^'):
			invert = True
			filename_or_image = filename_or_image[1:]

		im = Image.open(filename_or_image)
		if colorspace:
			im = im.convert(colorspace)
			
		if invert:
			invert_image(im)
		return im
	return filename_or_image



def read_text(filename):
	with open(filename, 'r') as f:
		return ''.join(f.readlines())

def read_json(filename, strict=True):
	if not strict:
		if not filename:
			return {}
		try:
			return read_json(filename, strict=True)
		except:
			pass
		try:
			with open(filename, 'r') as f:
				result = []
				for line in f.readlines():
					line = line.strip()
					if not line:
						continue
					result.append(line)
				content = ''.join(result)
				if content:
					return eval(content)
				else:
					return {}
		except:
			pass
		return {}
	else:
		return json.loads(read_text(filename))

def write_text(filename, content):
	with open(filename, 'w') as f:
		f.write(content)

def write_json(filename, content, readable=False):
	if readable:
		write_text(filename, json.dumps(content, indent=True, sort_keys=True))
	else:
		write_text(filename, json.dumps(content, separators=(',',':'), sort_keys=True))