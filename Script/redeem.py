import hashlib
import argparse


SECRET_KEY = "96C81420-BED7-497C-B13C-BA08276FFB7C"

def main():

    parser = argparse.ArgumentParser(description='Generate letterspace redeem url')
    parser.add_argument('--product', default='themepack')
    parser.add_argument('license')

    args = parser.parse_args()
    product = args.product
    license = args.license
    secret = SECRET_KEY

    data = '%(product)s!%(license)s!%(secret)s' % locals()
    redeem_code = hashlib.sha1(data.encode('utf-8')).hexdigest().upper()
    print('letterspace://redeem?%s' % redeem_code)

if __name__ == '__main__':
    main()