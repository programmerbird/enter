
import os, sys
from plistlib import *

def main():
    action = sys.argv[1]
    if action == '--get':
        (key, default, path) = sys.argv[2:]

        if os.path.exists(path):
            data = readPlist(path)
            print data.get(key, default)
        else:
            print default
    elif action == '--set':
        (key, value, path) = sys.argv[2:]
        if os.path.exists(path):
            data = readPlist(path)
        else:
            data = {}
        data[key] = value
        writePlist(data, path)
    
if __name__ == '__main__':
    main()    
