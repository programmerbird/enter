from PIL import Image



def main():
    im = Image.new('RGBA', (256, 256))
    pixels = im.load()
    for x in range(256):
        for y in range(256):
            pixels[x, y] = (x, x, x, y)
    im.save('gray.png')

    im = Image.new('RGBA', (512, 512))
    pixels = im.load()
    for x in range(256):
        for y in range(256):
            pixels[x*2, y*2] = (x, x, x, y)
            pixels[x*2+1, y*2] = (x, x, x, y)
            pixels[x*2, y*2+1] = (x, x, x, y)
            pixels[x*2+1, y*2+1] = (x, x, x, y)
    im.save('gray@2x.png')

if __name__ == '__main__':
    main()