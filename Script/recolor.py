#!/usr/bin/env python
#-*- coding:utf-8 -*-

import sys
import os
try:
    from PIL import Image 
except ImportError:
    import Image
from ui import ProgressBar

HEX = '0123456789abcdef'
HEXDICT = dict([(x, i) for (i, x) in enumerate(HEX)])

def hex2color(hexcode):
    m = [ HEXDICT[h] for h in hexcode.lower() ]
    l = len(m)
    if l == 3:
        return (m[0] * 17, m[1] * 17, m[2] * 17)
    elif l == 6:
        return (m[0]*16 + m[1], m[2]*16 + m[3], m[4]*16 + m[5])

def color2key(color):
    (r,g,b) = color
    return (r << 16) + (g << 8) + b 

def get_color_family(color):
    (r,g,b) = color
    if r >= g >= b:
        return 1
    elif r >= b >= g:
        return 2
    elif g >= r >= b:
        return 3
    elif g >= b >= r:
        return 4
    elif b >= r >= g:
        return 5
    elif b >= g >= r:
        return 6

class ColorRule(object):

    def __init__(self, text):
        (a, b) = text.split(':', 1)
        if '-' in a:
            (from_hex, to_hex) = a.split('-', 1)
            self.source_min_color = (minr, ming, minb) = hex2color(from_hex)
            self.source_max_color = (maxr, maxg, maxb) =  hex2color(to_hex)
            self.source_delta_color = (maxr-minr, maxg-ming, maxb-minb)
            self.source_family = get_color_family(self.source_delta_color)

        else:
            self.source_min_color = hex2color(a)
            self.source_max_color = None

        if '-' in b:
            (from_hex, to_hex) = b.split('-', 1)
            self.target_min_color = (minr, ming, minb) = hex2color(from_hex)
            self.target_max_color = (maxr, maxg, maxb) = hex2color(to_hex)
            self.target_delta_color = (maxr-minr, maxg-ming, maxb-minb)
        else:
            self.target_min_color = hex2color(b)
            self.target_max_color = None

    def convert(self, color):
        ratio = 1
        (r, g, b) = color
        (mr, mg, mb) = self.source_min_color
        if self.source_max_color:
            (dr, dg, db) = self.source_delta_color

            r -= mr 
            g -= mg
            b -= mb
            if r < 0: return None
            if g < 0: return None
            if b < 0: return None

            if r > dr: return None
            if g > dg: return None
            if b > db: return None

            if self.source_family <= 2:
                # r dominated
                ratio = float(r) / float(dr)
            elif self.source_family <= 4:
                # g dominated
                ratio = float(g) / float(dg)
            elif self.source_family <= 6:
                # b dominated
                ratio = float(b) / float(db)
        else:
            if mr != r: return None
            if mg != g: return None
            if mb != b: return None
            ratio = 1


        if self.target_max_color:
            (tr, tg, tb) = self.target_min_color
            (dr, dg, db) = self.target_delta_color

            r = int(tr + float(dr) * ratio)
            g = int(tg + float(dg) * ratio)
            b = int(tb + float(db) * ratio)

            if r > 255: r = 255
            if g > 255: g = 255
            if b > 255: b = 255

            if r < 0: r = 0
            if g < 0: g = 0
            if b < 0: b = 0

            return (r, g, b)
        else:
            return self.target_min_color







def main():

    inputfile = sys.argv[-2]
    outputfile = sys.argv[-1]

    rules = []
    for color in sys.argv[1:-2]:
        rule = ColorRule(color)
        rules.append(rule)

    im = Image.open(inputfile).convert('RGBA')
    (w, h) = im.size 
    data = im.load()

    for y in range(h):
        for x in range(w):
            (r,g,b,a) = data[x, y]

            for rule in rules:
                color = rule.convert((r,g,b))
                if color:
                    (r, g, b) = color
                    data[x, y] = (r,g,b,a)
                    break

    im.save(outputfile)

    
if __name__ == '__main__':
    sys.exit(main())


