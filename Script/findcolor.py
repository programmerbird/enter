

def main():

	white_back = 255
	black_back = 0
	white_fore = 84
	black_fore = 18


	for r in range(256):
		for a in range(256):
			bg = white_back
			result = (r * a + bg * (255-a)) / 255

			if result != white_fore:
				continue

			bg = black_back
			result = (r * a + bg * (255-a)) / 255
			if result != black_fore:
				continue
			print('r1: %d a1: %d (%f)' % (r, a, float(a) / 255.0))

	print('------------')

	white_back = 84
	black_back = 18
	white_fore = 104
	black_fore = 53

	for r in range(256):
		for a in range(256):
			bg = white_back
			result = (r * a + bg * (255-a)) / 255

			if result != white_fore:
				continue

			bg = black_back
			result = (r * a + bg * (255-a)) / 255
			if result != black_fore:
				continue
			print('r2: %d a2: %d (%f)' % (r, a, float(a) / 255.0))

	print('------------')

	white_back = 84
	black_back = 18
	white_fore = 139
	black_fore = 90

	for r in range(256):
		for a in range(256):
			bg = white_back
			result = (r * a + bg * (255-a)) / 255

			if result != white_fore:
				continue

			bg = black_back
			result = (r * a + bg * (255-a)) / 255
			if abs(result - black_fore) > 4:
				continue
			print('r3: %d a3: %d (%f)' % (r, a, float(a) / 255.0))



if __name__ == '__main__':
	main()