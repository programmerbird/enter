#
#  genwheel.py
#  Colors
#
#  Created by Bird on 6/27/09.
#  Copyright (c) 2009 Extend Interactive. All rights reserved.
#


import sys, os
import math
import numpy
from PIL import Image, ImageOps
from utils import parameters, hex2color



def invert_image(im):
    im_num = numpy.asarray(im).copy()
    im_num[:,:,0:3] = 255 - im_num[:,:,0:3]
    return Image.fromarray(im_num)


def image(filename_or_image, colorspace='RGBA'):
    from PIL import Image
    if isinstance(filename_or_image, str):

        invert = False
        if filename_or_image.startswith('^'):
            invert = True
            filename_or_image = filename_or_image[1:]

        im = Image.open(filename_or_image)
        if colorspace:
            im = im.convert(colorspace)
        if invert:
            im = invert_image(im)
        return im
    return filename_or_image


def mask_picture(picture, mask):

    im = image(picture)
    mask = image(mask)

    im_num = numpy.uint16(numpy.asarray(im))
    mask_num = numpy.asarray(mask)

    im_num[:,:,3] = im_num[:,:,3] * mask_num[:,:,1] / 255

    im_num = numpy.uint8(im_num)
    return Image.fromarray(im_num)


def calibrated_mask_picture(picture, mask, calibrator, color):
    im = image(picture)
    mask = image(mask)

    red = image(os.path.join(calibrator, color[0:2].lower() + '.png'))
    green = image(os.path.join(calibrator, color[2:4].lower() + '.png'))
    blue = image(os.path.join(calibrator, color[4:6].lower() + '.png'))

    (w, h) = mask.size

    mm = mask.load()
    ss = im.load()

    cc = [
        red.load(),
        green.load(),
        blue.load(),
    ]

    step = 1

    (red_width, _) = red.size
    if red_width >= 512:
        step = 2

    blank = (0, 0, 0, 0)
    for y in range(h):
        for x in range(w):
            opacity = mm[x, y][0]
            source = ss[x, y]

            if opacity == 0:
                ss[x, y] = blank

            else:
                row = opacity * step
                color = [0,0,0]
                for d in range(3):
                    source_d = source[d]
                    nearest = 0
                    nearest_diff = 1000
                    for cursor in range(256):
                        output = cc[d][cursor * step, row][d]
                        diff = abs(source_d - output)
                        if nearest_diff > diff:
                            nearest = cursor
                            nearest_diff = diff
                        elif nearest_diff == diff and abs(nearest - source_d) < abs(cursor - source_d):
                            nearest = cursor
                            nearest_diff = diff
                    color[d] = nearest
                ss[x, y] = (color[0], color[1], color[2], opacity)

    return im


def blend_picture(picture, overlay, color):
    im = image(picture)
    overlay = image(overlay)

    (r,g,b) = color 

    im_num = numpy.uint16(numpy.asarray(im))
    overlay_num = numpy.uint16(numpy.asarray(overlay))

    im_num[:,:,0] *= (255-r)
    im_num[:,:,1] *= (255-g)
    im_num[:,:,2] *= (255-b)

    overlay_num[:,:,0] *= r
    overlay_num[:,:,1] *= g
    overlay_num[:,:,2] *= b

    im_num[:,:,0:3] = (im_num[:,:,0:3] + overlay_num[:,:,0:3]) / 255

    im_num = numpy.uint8(im_num)
    return Image.fromarray(im_num)


def paste_picture(picture, overlay, x=0, y=0):

    x = int(x)
    y = int(y)

    im = image(picture)
    overlay = image(overlay)

    im_num = numpy.uint32(numpy.asarray(im))
    overlay_num = numpy.uint32(numpy.asarray(overlay))

    (im_h, im_w, _) = im_num.shape

    ox = 0
    oy = 0
    (overlay_h, overlay_w, _) = overlay_num.shape

    if y < 0:
        oy = -y
        overlay_h -= oy
        y = 0
    if x < 0:
        ox = -x
        overlay_w -= ox
        x = 0

    if y + overlay_h > im_h:
        overlay_h = im_h - y

    if x + overlay_w > im_w:
        overlay_w = im_w - x

    ey = y + overlay_h
    ex = x + overlay_w

    overlay_num = overlay_num[oy:oy+overlay_h,ox:ox+overlay_w,:]


    overlay_alpha = overlay_num[:,:,3]

    im_alpha = im_num[y:ey, x:ex, 3]
    im_alpha = numpy.minimum(im_alpha, 255-overlay_alpha) 

    im_num[y:ey,x:ex,0] = numpy.multiply(im_num[y:ey,x:ex, 0], im_alpha) 
    im_num[y:ey,x:ex,1] = numpy.multiply(im_num[y:ey,x:ex, 1], im_alpha) 
    im_num[y:ey,x:ex,2] = numpy.multiply(im_num[y:ey,x:ex, 2], im_alpha) 
    im_num[y:ey,x:ex,3] = im_alpha

    overlay_num[:,:,0] = numpy.multiply(overlay_num[:,:,0], overlay_alpha)
    overlay_num[:,:,1] = numpy.multiply(overlay_num[:,:,1], overlay_alpha)
    overlay_num[:,:,2] = numpy.multiply(overlay_num[:,:,2], overlay_alpha)

    im_num[y:ey,x:ex,:] += overlay_num[:,:,:]
    im_alpha = im_num[y:ey, x:ex, 3]

    im_num[y:ey,x:ex,0] = numpy.divide(im_num[y:ey,x:ex,0], im_alpha)
    im_num[y:ey,x:ex,1] = numpy.divide(im_num[y:ey,x:ex,1], im_alpha)
    im_num[y:ey,x:ex,2] = numpy.divide(im_num[y:ey,x:ex,2], im_alpha)
    im_num[im_num==numpy.inf] = 0


    im_num = numpy.uint8(im_num)
    return Image.fromarray(im_num)


def get_size(params):
    rotate = params.get('rotate') or False
    if isinstance(rotate, str):
        if rotate.lower() in ('n', 'f', 'false', '0', 'no'):
            rotate = False
    template = params.get('template')
    w = 0
    h = 0
    if template:
        template = image(template)
        (w, h) = template.size
    if params.get('size'):
        w = h = int(params.get('size'))
    if params.get('width'):
        w = int(params.get('width'))
    if params.get('height'):
        h = int(params.get('height'))
    return dict(size=(w,h), rotate=rotate)


def resize_aspect_fill(picture, size, rotate=False):
    (tw, th) = size

    picture = image(picture)
    (pw, ph) = picture.size

    if rotate:
        picture_is_horizontal = (pw >= ph)
        template_is_horizontal = (tw >= th)
        if picture_is_horizontal != template_is_horizontal:
            size = (th, tw)

    picture = ImageOps.fit(picture, size, Image.ANTIALIAS, 0, (0.5, 0.5))    
    return picture

def resize_aspect_fit(picture, size, rotate=False):
    picture = image(picture)
    pw, ph = picture.size
    tw, th = size

    if rotate:
        picture_is_horizontal = (pw >= ph)
        template_is_horizontal = (tw >= th)
        if picture_is_horizontal != template_is_horizontal:
            size = (th, tw)
            tw, th = size

    picture.thumbnail(size, Image.ANTIALIAS)
    return picture



def unretina(filename):
    im = image(filename)
    (w, h) = im.size
    return im.resize((int(w/2),int(h/2)), Image.ANTIALIAS)
    # return ImageOps.fit(im, (w/2,h/2), Image.ANTIALIAS, 0, (0.5, 0.5))        


def main():

    args = sys.argv[1:]
    total = len(args)
    index = 0

    im = None
    while index < total:
        action = args[index]
        index += 1

        kwargs = {}
        sargs = []

        while index < total:
            arg = args[index]
            if arg == '--':
                index += 1
                break

            if arg.startswith('-'):
                kwargs[arg[1:]] = args[index + 1]
                index += 2

            else:
                sargs.append(arg)
                index += 1

        print('=>', action, sargs or '', kwargs or '')

        if action == 'new':
            if kwargs.get('color'):
                im = Image.new('RGBA', get_size(kwargs)['size'], color=hex2color(kwargs['color']))
            else:
                im = Image.new('RGBA', get_size(kwargs)['size'], color=(0,0,0,0))

        elif action == 'mask':
            im = mask_picture(im, *sargs, **kwargs)

        elif action == 'calibratedmask':
            im = calibrated_mask_picture(im, *sargs, **kwargs)            

        elif action == 'paste':
            im = paste_picture(im, *sargs, **kwargs)

        elif action == 'paint':
            if kwargs.get('color'):
                kwargs['overlay'] = Image.new('RGBA', get_size(kwargs)['size'], color=hex2color(kwargs['color']))
                del kwargs['color']
                del kwargs['width']
                del kwargs['height']
                
            im = paste_picture(im, *sargs, **kwargs)

        elif action == 'pastemask':
            mask = image(kwargs['mask'])
            n = Image.new('RGBA', mask.size, color=hex2color(kwargs['color']))
            n = mask_picture(n, mask)
            del kwargs['mask']
            del kwargs['color']
            kwargs['overlay'] = n

            im = paste_picture(im, *sargs, **kwargs)

        elif action == 'canvas':
            x = int(kwargs.get('x', 0))
            y = int(kwargs.get('y', 0))
            (w, h) = get_size(kwargs)['size']
            if w == 0:
                w = im.size[0] + x
            if h == 0:
                h = im.size[1] + y                
            bg = Image.new('RGBA', (w, h), color=(0,0,0,0))
            print(x,y,w,h)
            im = paste_picture(bg, im, x=x, y=y)

        elif action == 'background':
            if sargs:
                bg = image(sargs[0])
            elif kwargs.get('color'):
                bg = Image.new('RGBA', im.size, color=hex2color(kwargs['color']))
            else:
                raise ValueError('missing arguments')
            im = paste_picture(bg, im)

        elif action == 'transpose':
            # FLIP_LEFT_RIGHT
            # FLIP_TOP_BOTTOM
            # ROTATE_90
            # ROTATE_180
            # ROTATE_270
            im = im.transpose(getattr(Image, sargs[0].upper()))

        elif action == 'aspectfit':
            im = resize_aspect_fit(im, **get_size(kwargs))

        elif action == 'aspectfill':
            im = resize_aspect_fill(im, **get_size(kwargs))

        elif action == 'resize':
            im = im.resize(get_size(kwargs)['size'], getattr(Image, kwargs.get('strategy', 'ANTIALIAS')))

        elif action == 'unretina':
            im = unretina(im)

        elif action == 'blendcolor':
            im = blend_picture(im, kwargs['picture'], color=hex2color(kwargs['color']))

        elif action == 'blur':
            from PIL import ImageFilter
            im = im.filter(ImageFilter.BLUR)

        elif action == 'invert':
            im = invert_image(im)

        elif action == 'capinset':
            from capinset import inset_resize, get_size as cap_get_size, get_inset
            im = inset_resize(im, size=cap_get_size(kwargs), inset=get_inset(kwargs), threshold=int(kwargs.get('threshold', 10)))

        elif action == 'removestatusbartime':
            (w, h) = im.size
            topleft = im.load()[0,0]
            overlay = Image.new('RGBA', (120, 40), color=topleft)
            im = paste_picture(im, overlay, x=(w-120)/2, y=0)

        elif action == 'concat':
            (w, h) = im.size

            padding = int(kwargs.get('padding', 0))
            picture = image(kwargs['picture'])
            (pw, ph) = picture.size

            bg = Image.new('RGBA', (w+padding+pw, max(h, ph)), color=(255,255,255,255))
            bg = paste_picture(bg, im, x=0, y=0)
            bg = paste_picture(bg, picture, x=w+padding, y=0)
            im = bg

        elif not im and os.path.exists(action):
            im = image(action)

        elif not im and action.startswith('^') and os.path.exists(action[1:]):
            im = image(action)

        elif not kwargs and not sargs:
            im.save(action)

        else:
            raise NotImplementedError("unknow action: %s %s %s" % (action, kwargs, sargs))


    
if __name__ == '__main__':
    main()