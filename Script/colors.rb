
MARKS = [
    [0.0, 0.0],
    [22.5, 13.0],
    [45.0, 26.0],
    [67.5, 38.0],
    [90.0, 47.0],
    [115.5, 56.0],
    [135.0, 78.0],
    [160.5, 110.0],
    [180.0, 137.0],
    [202.5, 162.0],
    [225.0, 187.0],
    [247.5, 211.0],
    [270.0, 235.0],
    [292.5, 259.0],
    [315.0, 284.0], 
    [337.5, 315.0], 
    [360.0, 360.0],
]


HEXES = '0123456789ABCDEF'

def blendcolor(hex1, hex2)
    hex = hex1.upcase + 'FF'
    r1 = HEXES.index(hex[0]) * 16 + HEXES.index(hex[1])
    g1 = HEXES.index(hex[2]) * 16 + HEXES.index(hex[3])
    b1 = HEXES.index(hex[4]) * 16 + HEXES.index(hex[5])
    a1 = HEXES.index(hex[6]) * 16 + HEXES.index(hex[7])

    hex = hex2.upcase + 'FF'
    r2 = HEXES.index(hex[0]) * 16 + HEXES.index(hex[1])
    g2 = HEXES.index(hex[2]) * 16 + HEXES.index(hex[3])
    b2 = HEXES.index(hex[4]) * 16 + HEXES.index(hex[5])
    a2 = HEXES.index(hex[6]) * 16 + HEXES.index(hex[7])

    left_a1 = 255 - a2
    if left_a1 > a1 then
        left_a1 = a1
    end

    a = a2 + left_a1
    r = (a2 * r2 + left_a1 * r1) / a
    g = (a2 * g2 + left_a1 * g1) / a 
    b = (a2 * b2 + left_a1 * b1) / a

    hex_r = HEXES[r / 16] + HEXES[r % 16]
    hex_g = HEXES[g / 16] + HEXES[g % 16]
    hex_b = HEXES[b / 16] + HEXES[b % 16]
    hex_a = HEXES[a / 16] + HEXES[a % 16]

    return "#{hex_r}#{hex_g}#{hex_b}#{hex_a}"
end

def angle2hue(angle)
    while angle < 0 do 
        angle += 360.0
    end
    while angle > 360.0 do
        angle -= 360.0
    end
    last_a = 0
    last_b = 0
    low_a = 0
    low_b = 0
    a = 0
    b = 0
    MARKS.each do |chunk|
        a = chunk[0]
        b = chunk[1]
        if a == angle then
            return b 
        end
        if a > angle then
            break
        end
        low_a = a
        low_b = b
    end
    da = (a - low_a).to_f
    if da <= 0 then
       return low_b 
    end
    return (angle - low_a).to_f / da * (b - low_b).to_f + low_b
end

def hue2angle(angle)
    while angle < 0 do 
        angle += 360.0
    end
    while angle > 360.0 do
        angle -= 360.0
    end
    
    last_a = 0
    last_b = 0
    low_a = 0
    low_b = 0
    a = 0
    b = 0
    MARKS.each do |chunk|
        b = chunk[0]
        a = chunk[1]
        if a == angle then
            return b 
        end
        if a > angle then
            break
        end
        low_a = a
        low_b = b
    end
    da = (a - low_a).to_f
    if da <= 0 then
       return low_b 
    end
    return (angle - low_a).to_f / da * (b - low_b).to_f + low_b
end

def schema2hsv(schema)
    hue = angle2hue(schema[0])
    return [hue, schema[1], schema[2]]
end

def hsv2schema(hsv)
    angle = hue2angle(hsv[0])
    return [angle, hsv[1], hsv[2]]
end


def hsv2rgb(hsv)
    h = hsv[0].to_f / 360.0 
    s = hsv[1].to_f / 100.0
    v = hsv[2].to_f / 100.0    

    while h < 0 do
        h += 1.0
    end
    while h > 1.0 do
        h -= 1.0
    end

    if s < 0 then
        s = 0.0
    end
    if s > 1 then
        s = 1.0
    end
    if v < 0 then 
        v = 0.0
    end
    if v > 1 then
        v = 1.0
    end
    if s == 0 then
        return [v * 255.0,v * 255.0,v * 255.0]
    end

    var_h = h * 6
    var_i = var_h.floor
    var_1 = v * ( 1.0 - s )
    var_2 = v * ( 1.0 - s * ( var_h - var_i ) )
    var_3 = v * ( 1.0 - s * ( 1.0 - ( var_h - var_i ) ) )
    
    v *= 255.0
    var_1 *= 255.0
    var_2 *= 255.0
    var_3 *= 255.0
    if var_i == 0 then
        return [v, var_3, var_1]
    elsif var_i == 1 then
        return [var_2, v, var_1]
    elsif var_i == 2 then
        return [var_1, v, var_3]
    elsif var_i == 3 then
        return [var_1, var_2, v]
    elsif var_i == 4 then
        return [var_3, var_1, v]
    else
        return [v, var_1, var_2]
    end
end


def rgb2hsv(rgb)
    r = rgb[0].to_f / 255.0
    g = rgb[1].to_f / 255.0
    b = rgb[2].to_f / 255.0

    h = 0
    s = 0
    v = 0

    maxc = [r,g,b].max
    minc = [r,g,b].min
    dmax = maxc - minc
    
    v = maxc
    if dmax==0 then
        h = 0
        s = 0
    else
        s = dmax / maxc
        dr = ( ((maxc - r) / 6.0 ) + (dmax / 2.0) ) / maxc
        dg = ( ((maxc - g) / 6.0 ) + (dmax / 2.0) ) / maxc
        db = ( ((maxc - b) / 6.0 ) + (dmax / 2.0) ) / maxc
        
        if r==maxc then
            h = db - dg
        elsif g==maxc
            h = (1.0/3.0) + dr - db
        elsif b==maxc
            h = (2.0/3.0) + dg - dr
        end
        
        while h<0 do
            h+=1.0
        end
        while h>1 do
            h-=1.0
        end
    end
    return [h*360.0, s*100.0, v*100.0]
end


def clamprgb(r)
    if r < 0 then
        return 0
    end
    if r > 255 then
        return 255
    end
    return r
end

def rgb2hex(rgb)
    hexes = '0123456789abcdef'
    r = clamprgb(rgb[0].round)
    g = clamprgb(rgb[1].round)
    b = clamprgb(rgb[2].round)

    hex_r = hexes[r / 16] + hexes[r % 16]
    hex_g = hexes[g / 16] + hexes[g % 16]
    hex_b = hexes[b / 16] + hexes[b % 16]
    return "#{hex_r}#{hex_g}#{hex_b}"
end

def hex2rgb(hex)
    hexes = '0123456789abcdef'
    hex = hex.downcase
    r = hexes.index(hex[0]) * 16 + hexes.index(hex[1])
    g = hexes.index(hex[2]) * 16 + hexes.index(hex[3])
    b = hexes.index(hex[4]) * 16 + hexes.index(hex[5])
    return [r,g,b]
end

def schema2hex(schema)
    return rgb2hex(hsv2rgb(schema2hsv(schema)))
end

def hex2schema(hex)
    return hsv2schema(rgb2hsv(hex2rgb(hex)))
end

def deltargb(rgb1, rgb2)
    dr = rgb1[0] - rgb2[0]
    dg = rgb1[1] - rgb2[1]
    db = rgb1[2] - rgb2[2]
    return dr * dr + dg * dg + db * db
end
def findpeppermint(hex)
    rgb = hex2rgb(hex)    
    hsv = rgb2hsv(rgb)
    estimate_s = hsv[1].round.to_i - 5
    estimate_v = hsv[2].round.to_i - 5

    if estimate_s < 0 then
        estimate_s = 0
    elsif estimate_s > 90 then
        estimate_s = 90
    end
    
    if estimate_v < 0 then
        estimate_v = 0
    elsif estimate_v > 90 then
        estimate_v = 90
    end


    nearest = [0,0,0]
    nearest_distance = deltargb(rgb, nearest)

    for a in 0..360
        for s in estimate_s..(estimate_s+10)
            for v in estimate_v..(estimate_v+10)
                schema = [a,s,v]
                current = hsv2rgb(schema2hsv(schema))
                distance = deltargb(current, rgb)
                if nearest_distance >= distance then
                    nearest_distance = distance
                    nearest = schema
                end
            end
        end
    end
    return nearest
end

def peppermint(a,s,v)
    return schema2hex([a,s,v])    
end

if __FILE__ == $0
    hex = ARGV[0]

    schema = findpeppermint(hex)
    puts()
    newhex = schema2hex(schema)
    puts("peppermint(#{schema[0]}, #{schema[1]}, #{schema[2]}), \##{newhex}")
    puts()
end

