#!/usr/bin/env

import sys
from PIL import Image
from utils import parameters, image

def color_diff(a, b):
	ar,ag,ab,aa = a 
	br,bg,bb,ba = b
	return abs(ar-br) + abs(ag-bg) + abs(ab-bb) + abs(aa-ba)

def find_horizontal_inset(im, threshold=10):
	(w, h) = im.size
	data = im.load()
	max_index = 0
	max_result = 0
	result = [0]
	for x in range(1, w):
		ok = True
		for y in range(h):
			if color_diff(data[x,y], data[x-1,y]) > threshold:
				ok = False
				break
		if ok:
			r = result[-1] + 1
			if r > max_result:
				max_result = r
				max_index = x
			result.append(r)
		else:
			result.append(0)
	return (max_index-max_result+1, w-max_index)

def find_vertical_inset(im, threshold=10):
	(w, h) = im.size
	data = im.load()
	max_index = 0
	max_result = 0
	result = [0]
	for y in range(1, h):
		ok = True
		for x in range(w):
			if color_diff(data[x,y], data[x,y-1]) > threshold:
				ok = False
				break
		if ok:
			r = result[-1] + 1
			if r > max_result:
				max_result = r
				max_index = y
			result.append(r)
		else:
			result.append(0)
	return (max_index - max_result+1, h-max_index)

def inset_resize(im, size, inset=None, threshold=10):
	im = image(im)
	if not inset:
		(l, r) = find_horizontal_inset(im, threshold=threshold)
		(t, b) = find_vertical_inset(im, threshold=threshold)
		inset=(t,r,b,l)

	data = im.load()

	result = []
	(w, h) = size

	(st,sr,sb,sl) = inset
	(sw, sh) = im.size

	for y in range(h):
		for x in range(w):
			left = x
			right = w - x
			top = y
			bottom = h - y
			if left < sl and right < sr:
				if left < right:
					sx = x
				else:
					sx = sw - right
			elif left < sl:
				sx = x
			elif right < sr:
				sx = sw - right
			else:
				sx = sl + 1

			if top < st and bottom < sb:
				if top < bottom:
					sy = y
				else:
					sy = sh - bottom
			elif top < st:
				sy = y
			elif bottom < sb:
				sy = sh - bottom
			else:
				sy = st + 1

			result.append(data[sx, sy])

	output = Image.new('RGBA', size)
	output.putdata(result)
	return output

def get_size(params):
	w = 0
	h = 0
	if params.get('size'):
		w = h = int(params.get('size'))
	if params.get('width'):
		w = int(params.get('width'))
	if params.get('height'):
		h = int(params.get('height'))
	if not w or not h:
		im = image(params.get('picture'))
		(sw, sh) = im.size
		if not w:
			w = sw
		if not h:
			h = sh

	return (w, h)

def get_inset(params):
	inset = params.get('inset')
	if not inset:
		return None

	chunks = inset.strip().split(',') 
	total = len(chunks)

	args = [ int(x) for x in chunks ]
	if total == 1:
		a = args[0]
		return (a,a,a,a)
	elif total == 2:
		a = args[0]
		b = args[1]
		return (a,b,a,b)
	elif total == 3:
		a = args[0]
		b = args[1]
		c = args[2]
		return (a,b,c,b)
	elif total == 4:
		return args


def main():
	args, params = parameters()
	action = args[1]
	output = args[2]

	if action == 'resize':
		picture = params.get('picture')
		threshold = params.get('threshold', 10)
		picture = inset_resize(picture, size=get_size(params), inset=get_inset(params), threshold=int(threshold))
		picture.save(output)

if __name__ == '__main__':
	main()